<?php 

$language_id = 2;
foreach($data['languages'] as $language) {
	if($language['language_id'] != 1) {
		$language_id = $language['language_id'];
	}
}

$output = array();
$output["advanced_grid_module"] = array (
  1 => 
  array (
    'custom_class' => '',
    'margin_top' => '0',
    'margin_right' => '0',
    'margin_bottom' => '0',
    'margin_left' => '0',
    'padding_top' => '0',
    'padding_right' => '0',
    'padding_bottom' => '0',
    'padding_left' => '0',
    'force_full_width' => '1',
    'background_color' => '',
    'background_image_type' => '2',
    'background_image' => 'catalog/home07-mg-block8.jpg',
    'background_image_position' => 'top center',
    'background_image_repeat' => 'no-repeat',
    'background_image_attachment' => 'scroll',
    'layout_id' => '1',
    'position' => 'content_bottom',
    'status' => '1',
    'sort_order' => '15',
    'disable_on_mobile' => '0',
    'column' => 
    array (
      1 => 
      array (
        'status' => '1',
        'width' => '12',
        'disable_on_mobile' => '0',
        'width_xs' => '1',
        'width_sm' => '1',
        'width_md' => '1',
        'width_lg' => '1',
        'sort' => '1',
        'module' => 
        array (
          1 => 
          array (
            'status' => '1',
            'sort' => '1',
            'type' => 'html',
            'html' => 
            array (
              $language_id => '<div class="block-static row">
     <div class="block-wrap">
          <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
               <div class="interior-block-static">
                    <div class="interior-inner">
                         <h3 class="title">unique &amp; morden</h3>
                         <div class="text hidden-xs">We alway focus on create unique and morden design for each other products. With elements simple,clean and minimalist, user experience of client will become easiet.</div>
                         <div class="author"><strong>C-KNIGHTZ ART</strong> - CEO Founder</div>
                    </div>
               </div>
          </div>
     </div>
</div>',
              1 => '<div class="block-static row">
     <div class="block-wrap">
          <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
               <div class="interior-block-static">
                    <div class="interior-inner">
                         <h3 class="title">unique &amp; morden</h3>
                         <div class="text hidden-xs">We alway focus on create unique and morden design for each other products. With elements simple,clean and minimalist, user experience of client will become easiet.</div>
                         <div class="author"><strong>C-KNIGHTZ ART</strong> - CEO Founder</div>
                    </div>
               </div>
          </div>
     </div>
</div>',
            ),
          ),
        ),
      ),
    ),
  ),
  2 => 
  array (
    'custom_class' => '',
    'margin_top' => '0',
    'margin_right' => '0',
    'margin_bottom' => '0',
    'margin_left' => '0',
    'padding_top' => '0',
    'padding_right' => '0',
    'padding_bottom' => '0',
    'padding_left' => '0',
    'force_full_width' => '1',
    'background_color' => '',
    'background_image_type' => '0',
    'background_image' => '',
    'background_image_position' => 'top left',
    'background_image_repeat' => 'no-repeat',
    'background_image_attachment' => 'scroll',
    'layout_id' => '1',
    'position' => 'content_bottom',
    'status' => '1',
    'sort_order' => '5',
    'disable_on_mobile' => '0',
    'column' => 
    array (
      2 => 
      array (
        'status' => '1',
        'width' => '12',
        'disable_on_mobile' => '0',
        'width_xs' => '1',
        'width_sm' => '1',
        'width_md' => '1',
        'width_lg' => '1',
        'sort' => '1',
        'module' => 
        array (
          1 => 
          array (
            'status' => '1',
            'sort' => '1',
            'type' => 'html',
            'html' => 
            array (
              $language_id => '<div class="block-static row">
     <div class="block-wrap">
          <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding">
                    <div class="interior-block-slide" style="background-image: url(image/catalog/home07-img-block4.jpg);">
                         <div class="interior-inner">
                              <h4 class="title"><a href="#">box bookshelf <span class="price" style="color: #cc9900;font-family: Lato">$65</span></a></h4>
                              <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                         </div>
                    </div>
               </div>
          
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding">
                    <div class="interior-block-slide" style="background-image: url(image/catalog/home07-img-block5.jpg);">
                         <div class="interior-inner">
                              <h4 class="title"><a href="#">bowl decor <span class="price" style="color: #cc9900;font-family: Lato">$25</span></a></h4>
                              <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                         </div>
                    </div>
               </div>
               
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding">
                    <div class="interior-block-slide" style="background-image: url(image/catalog/home07-img-block6.jpg);">
                         <div class="interior-inner">
                              <h4 class="title"><a href="#">storage stone <span class="price" style="color: #cc9900;font-family: Lato">$40</span></a></h4>
                              <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>',
              1 => '<div class="block-static row">
     <div class="block-wrap">
          <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding">
                    <div class="interior-block-slide" style="background-image: url(image/catalog/home07-img-block4.jpg);">
                         <div class="interior-inner">
                              <h4 class="title"><a href="#">box bookshelf <span class="price" style="color: #cc9900;font-family: Lato">$65</span></a></h4>
                              <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                         </div>
                    </div>
               </div>
          
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding">
                    <div class="interior-block-slide" style="background-image: url(image/catalog/home07-img-block5.jpg);">
                         <div class="interior-inner">
                              <h4 class="title"><a href="#">bowl decor <span class="price" style="color: #cc9900;font-family: Lato">$25</span></a></h4>
                              <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                         </div>
                    </div>
               </div>
               
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding">
                    <div class="interior-block-slide" style="background-image: url(image/catalog/home07-img-block6.jpg);">
                         <div class="interior-inner">
                              <h4 class="title"><a href="#">storage stone <span class="price" style="color: #cc9900;font-family: Lato">$40</span></a></h4>
                              <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>',
            ),
          ),
        ),
      ),
    ),
  ),
  3 => 
  array (
    'custom_class' => '',
    'margin_top' => '0',
    'margin_right' => '0',
    'margin_bottom' => '0',
    'margin_left' => '0',
    'padding_top' => '0',
    'padding_right' => '0',
    'padding_bottom' => '0',
    'padding_left' => '0',
    'force_full_width' => '1',
    'background_color' => '',
    'background_image_type' => '2',
    'background_image' => 'catalog/home07-img-block3.jpg',
    'background_image_position' => 'bottom center',
    'background_image_repeat' => 'no-repeat',
    'background_image_attachment' => 'scroll',
    'layout_id' => '1',
    'position' => 'content_bottom',
    'status' => '1',
    'sort_order' => '0',
    'disable_on_mobile' => '0',
    'column' => 
    array (
      3 => 
      array (
        'status' => '1',
        'width' => '12',
        'disable_on_mobile' => '0',
        'width_xs' => '1',
        'width_sm' => '1',
        'width_md' => '1',
        'width_lg' => '1',
        'sort' => '1',
        'module' => 
        array (
          1 => 
          array (
            'status' => '1',
            'sort' => '1',
            'type' => 'html',
            'html' => 
            array (
              $language_id => '     <div class="block-static row ">
          <div class="block-wrap">
               <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                    <div class="interior-block-static">
                         <div class="interior-inner">
                              <h3 class="title">ilian bottle <span class="price" style="font-family: Lato;color: #cc9900">$45</span></h3>
                              <div class="text hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. sque sed scelerisque arcu. Sed vel luctus elit, at ultricies odion tuniomi loi. Aliquam vehicula.</div>
                              <div class="hidden-xs"><span>&nbsp;<span class="fa fa-caret-right">&nbsp;</span>&nbsp;</span>Sed vel luctus elit, at ultricies odion</div>
                              <div class="text hidden-xs"><span>&nbsp;<span class="fa fa-caret-right">&nbsp;</span>&nbsp;</span>Lorem ipsum dolor sit amet consectetur</div>
                              <div class="btn-view"><a class="btn-readmore hover-effect07" href="#"><span>View Now !</span></a></div>
                         </div>
                    </div>
               </div>
          </div>
     </div>',
              1 => '     <div class="block-static row ">
          <div class="block-wrap">
               <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                    <div class="interior-block-static">
                         <div class="interior-inner">
                              <h3 class="title">ilian bottle <span class="price" style="font-family: Lato;color: #cc9900">$45</span></h3>
                              <div class="text hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. sque sed scelerisque arcu. Sed vel luctus elit, at ultricies odion tuniomi loi. Aliquam vehicula.</div>
                              <div class="hidden-xs"><span>&nbsp;<span class="fa fa-caret-right">&nbsp;</span>&nbsp;</span>Sed vel luctus elit, at ultricies odion</div>
                              <div class="text hidden-xs"><span>&nbsp;<span class="fa fa-caret-right">&nbsp;</span>&nbsp;</span>Lorem ipsum dolor sit amet consectetur</div>
                              <div class="btn-view"><a class="btn-readmore hover-effect07" href="#"><span>View Now !</span></a></div>
                         </div>
                    </div>
               </div>
          </div>
     </div>',
            ),
          ),
        ),
      ),
    ),
  ),
  4 => 
  array (
    'custom_class' => '',
    'margin_top' => '0',
    'margin_right' => '0',
    'margin_bottom' => '0',
    'margin_left' => '0',
    'padding_top' => '0',
    'padding_right' => '0',
    'padding_bottom' => '0',
    'padding_left' => '0',
    'force_full_width' => '1',
    'background_color' => '',
    'background_image_type' => '0',
    'background_image' => '',
    'background_image_position' => 'top left',
    'background_image_repeat' => 'no-repeat',
    'background_image_attachment' => 'scroll',
    'layout_id' => '1',
    'position' => 'content_bottom',
    'status' => '1',
    'sort_order' => '7',
    'disable_on_mobile' => '0',
    'column' => 
    array (
      4 => 
      array (
        'status' => '1',
        'width' => '12',
        'disable_on_mobile' => '0',
        'width_xs' => '1',
        'width_sm' => '1',
        'width_md' => '1',
        'width_lg' => '1',
        'sort' => '1',
        'module' => 
        array (
          1 => 
          array (
            'status' => '1',
            'sort' => '1',
            'type' => 'html',
            'html' => 
            array (
              $language_id => '<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 no-padding interior-block-slide-wrap">
     <div class="block-static clearfix">
          <div class="owl-carousel owl-carousel-one owl-theme ">
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box3-item1.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">simple sofa <span class="price" style="font-family: Lato;color: #cc9900">$320</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box3-item2.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">simple sofa <span class="price" style="font-family: Lato;color: #cc9900">$300</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box3-item3.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">minimal sofa <span class="price" style="font-family: Lato;color: #cc9900">$200</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(".owl-carousel-one").owlCarousel({
            singleItem: true,
            navigation : true,
            direction: \'rtl\',
            navigationText : ["<i class=\'fa fa-angle-left\'></i>","<i class=\'fa fa-angle-right\'></i>"]
       });
    });
</script>

<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 no-padding">
     <div class="parallax-bg1" data-velocity="0.04" style="background-color: #e5e5e5;background-image: url(image/catalog/home07-img-block7.jpg);background-size: cover; background-position: bottom right; background-repeat: no-repeat;">
          <div class="block-static row">
               <div class="block-wrap">
                    <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                         <div class="interior-block-static">
                              <div class="interior-inner left">
                                   <h3 class="title">minimal sofa</h3>
                                   <div class="text hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. sque sed scelerisque arcu. Sed vel luctus elit, at ultricies odion tuniomi loi. Aliquam vehicula.</div>
                                   <div class="btn-view"><a class="btn-readmore hover-effect07" href="#"><span>View Now !</span></a></div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(\'.parallax-bg1\').scrolly({bgParallax: true});
    });
</script>',
              1 => '<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 no-padding interior-block-slide-wrap">
     <div class="block-static clearfix">
          <div class="owl-carousel owl-carousel-one owl-theme ">
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box3-item1.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">simple sofa <span class="price" style="font-family: Lato;color: #cc9900">$320</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box3-item2.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">simple sofa <span class="price" style="font-family: Lato;color: #cc9900">$300</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box3-item3.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">minimal sofa <span class="price" style="font-family: Lato;color: #cc9900">$200</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(".owl-carousel-one").owlCarousel({
            singleItem: true,
            navigation : true,
            navigationText : ["<i class=\'fa fa-angle-left\'></i>","<i class=\'fa fa-angle-right\'></i>"]
       });
    });
</script>

<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 no-padding">
     <div class="parallax-bg1" data-velocity="0.04" style="background-color: #e5e5e5;background-image: url(image/catalog/home07-img-block7.jpg);background-size: cover; background-position: bottom right; background-repeat: no-repeat;">
          <div class="block-static row">
               <div class="block-wrap">
                    <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                         <div class="interior-block-static">
                              <div class="interior-inner left">
                                   <h3 class="title">minimal sofa</h3>
                                   <div class="text hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. sque sed scelerisque arcu. Sed vel luctus elit, at ultricies odion tuniomi loi. Aliquam vehicula.</div>
                                   <div class="btn-view"><a class="btn-readmore hover-effect07" href="#"><span>View Now !</span></a></div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(\'.parallax-bg1\').scrolly({bgParallax: true});
    });
</script>',
            ),
          ),
        ),
      ),
    ),
  ),
  5 => 
  array (
    'custom_class' => '',
    'margin_top' => '0',
    'margin_right' => '0',
    'margin_bottom' => '0',
    'margin_left' => '0',
    'padding_top' => '0',
    'padding_right' => '0',
    'padding_bottom' => '0',
    'padding_left' => '0',
    'force_full_width' => '1',
    'background_color' => '',
    'background_image_type' => '0',
    'background_image' => '',
    'background_image_position' => 'top left',
    'background_image_repeat' => 'no-repeat',
    'background_image_attachment' => 'scroll',
    'layout_id' => '1',
    'position' => 'preface_fullwidth',
    'status' => '1',
    'sort_order' => '0',
    'disable_on_mobile' => '0',
    'column' => 
    array (
      5 => 
      array (
        'status' => '1',
        'width' => '12',
        'disable_on_mobile' => '0',
        'width_xs' => '1',
        'width_sm' => '1',
        'width_md' => '1',
        'width_lg' => '1',
        'sort' => '1',
        'module' => 
        array (
          1 => 
          array (
            'status' => '1',
            'sort' => '1',
            'type' => 'html',
            'html' => 
            array (
              $language_id => '<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 no-padding interior-block-slide-wrap">
     <div class="block-static clearfix">
          <div class="owl-carousel owl-carousel-two owl-theme ">
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box1-item1.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">bookshelf <span class="price" style="font-family: Lato;color: #cc9900">$80</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box1-item2.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">bookshelf <span class="price" style="font-family: Lato;color: #cc9900">$100</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box1-item3.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">bookshelf <span class="price" style="font-family: Lato;color: #cc9900">$120</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(".owl-carousel-two").owlCarousel({
            singleItem: true,
            navigation : true,
            direction: \'rtl\',
            navigationText : ["<i class=\'fa fa-angle-left\'></i>","<i class=\'fa fa-angle-right\'></i>"]
       });
    });
</script>

<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 no-padding">
     <div class="parallax-bg2" data-velocity="-0.05" style="background-color: #e5e5e5;background-image: url(image/catalog/home07-img-block1.jpg);background-size: cover; background-position: top left; background-repeat: no-repeat;">
          <div class="block-static row">
               <div class="block-wrap">
                    <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                         <div class="interior-block-static">
                              <div class="interior-inner block-center">
                                   <h3 class="title">Table collections</h3>
                                   <div class="text hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. sque sed scelerisque arcu. Sed vel luctus elit, at ultricies odion tuniomi loi. Aliquam vehicula.</div>
                                   <div class="btn-view"><a class="btn-readmore hover-effect07" href="#"><span>View Now !</span></a></div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(\'.parallax-bg2\').scrolly({bgParallax: true});
    });
</script>',
              1 => '<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 no-padding interior-block-slide-wrap">
     <div class="block-static clearfix">
          <div class="owl-carousel owl-carousel-two owl-theme ">
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box1-item1.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">bookshelf <span class="price" style="font-family: Lato;color: #cc9900">$80</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box1-item2.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">bookshelf <span class="price" style="font-family: Lato;color: #cc9900">$100</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box1-item3.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">bookshelf <span class="price" style="font-family: Lato;color: #cc9900">$120</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(".owl-carousel-two").owlCarousel({
            singleItem: true,
            navigation : true,
            navigationText : ["<i class=\'fa fa-angle-left\'></i>","<i class=\'fa fa-angle-right\'></i>"]
       });
    });
</script>

<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 no-padding">
     <div class="parallax-bg2" data-velocity="-0.05" style="background-color: #e5e5e5;background-image: url(image/catalog/home07-img-block1.jpg);background-size: cover; background-position: top left; background-repeat: no-repeat;">
          <div class="block-static row">
               <div class="block-wrap">
                    <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                         <div class="interior-block-static">
                              <div class="interior-inner block-center">
                                   <h3 class="title">Table collections</h3>
                                   <div class="text hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. sque sed scelerisque arcu. Sed vel luctus elit, at ultricies odion tuniomi loi. Aliquam vehicula.</div>
                                   <div class="btn-view"><a class="btn-readmore hover-effect07" href="#"><span>View Now !</span></a></div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(\'.parallax-bg2\').scrolly({bgParallax: true});
    });
</script>',
            ),
          ),
        ),
      ),
    ),
  ),
  6 => 
  array (
    'custom_class' => '',
    'margin_top' => '0',
    'margin_right' => '0',
    'margin_bottom' => '0',
    'margin_left' => '0',
    'padding_top' => '0',
    'padding_right' => '0',
    'padding_bottom' => '0',
    'padding_left' => '0',
    'force_full_width' => '1',
    'background_color' => '',
    'background_image_type' => '0',
    'background_image' => '',
    'background_image_position' => 'top left',
    'background_image_repeat' => 'no-repeat',
    'background_image_attachment' => 'scroll',
    'layout_id' => '1',
    'position' => 'preface_fullwidth',
    'status' => '1',
    'sort_order' => '2',
    'disable_on_mobile' => '0',
    'column' => 
    array (
      6 => 
      array (
        'status' => '1',
        'width' => '12',
        'disable_on_mobile' => '0',
        'width_xs' => '1',
        'width_sm' => '1',
        'width_md' => '1',
        'width_lg' => '1',
        'sort' => '1',
        'module' => 
        array (
          1 => 
          array (
            'status' => '1',
            'sort' => '1',
            'type' => 'html',
            'html' => 
            array (
              $language_id => '<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 no-padding">
     <div class="parallax-bg3" data-velocity="-0.04" style="background-color: #ececec;background-image: url(image/catalog/home07-img-block2.jpg);background-size: cover; background-position: top left; background-repeat: no-repeat;">
          <div class="block-static row">
               <div class="block-wrap">
                    <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                         <div class="interior-block-static">
                              <div class="interior-inner block-center">
                                   <h3 class="title">Pendant light</h3>
                                   <div class="text hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. sque sed scelerisque arcu. Sed vel luctus elit, at ultricies odion tuniomi loi. Aliquam vehicula.</div>
                                   <div class="btn-view"><a class="btn-readmore hover-effect07" href="#"><span>View Now !</span></a></div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(\'.parallax-bg3\').scrolly({bgParallax: true});
    });
</script>

<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 no-padding interior-block-slide-wrap" style="background: #f8f8f8;overflow: hidden">
     <div class="block-static clearfix">
          <div class="owl-carousel owl-carousel-three owl-theme ">
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box2-item1.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">Ceramic light <span class="price" style="font-family: Lato;color: #cc9900">$50</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box2-item2.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">Ceramic light <span class="price" style="font-family: Lato;color: #cc9900">$60</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box2-item3.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">Ceramic light <span class="price" style="font-family: Lato;color: #cc9900">$80</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(".owl-carousel-three").owlCarousel({
            singleItem: true,
            navigation : true,
            direction: \'rtl\',
            navigationText : ["<i class=\'fa fa-angle-left\'></i>","<i class=\'fa fa-angle-right\'></i>"]
       });
    });
</script>',
              1 => '<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 no-padding">
     <div class="parallax-bg3" data-velocity="-0.04" style="background-color: #ececec;background-image: url(image/catalog/home07-img-block2.jpg);background-size: cover; background-position: top left; background-repeat: no-repeat;">
          <div class="block-static row">
               <div class="block-wrap">
                    <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                         <div class="interior-block-static">
                              <div class="interior-inner block-center">
                                   <h3 class="title">Pendant light</h3>
                                   <div class="text hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. sque sed scelerisque arcu. Sed vel luctus elit, at ultricies odion tuniomi loi. Aliquam vehicula.</div>
                                   <div class="btn-view"><a class="btn-readmore hover-effect07" href="#"><span>View Now !</span></a></div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(\'.parallax-bg3\').scrolly({bgParallax: true});
    });
</script>

<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 no-padding interior-block-slide-wrap" style="background: #f8f8f8;overflow: hidden">
     <div class="block-static clearfix">
          <div class="owl-carousel owl-carousel-three owl-theme ">
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box2-item1.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">Ceramic light <span class="price" style="font-family: Lato;color: #cc9900">$50</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box2-item2.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">Ceramic light <span class="price" style="font-family: Lato;color: #cc9900">$60</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="item">
                    <div class="block-content clearfix">
                         <div class="interior-block-slide" style="background-image: url(image/catalog/home07-box2-item3.jpg);">
                              <div class="interior-inner">
                                   <h4 class="title"><a href="#">Ceramic light <span class="price" style="font-family: Lato;color: #cc9900">$80</span></a></h4>
                                   <div class="text">Lorem ipsum dolor sit amet, consectetur uilon adipis cing elit. Quisque sed scelerisque arcu.</div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
    $(document).ready(function(){
       $(".owl-carousel-three").owlCarousel({
            singleItem: true,
            navigation : true,
            navigationText : ["<i class=\'fa fa-angle-left\'></i>","<i class=\'fa fa-angle-right\'></i>"]
       });
    });
</script>',
            ),
          ),
        ),
      ),
    ),
  ),
); 

$this->model_setting_setting->editSetting( "advanced_grid", $output );	

?>