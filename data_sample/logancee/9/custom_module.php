<?php 

$language_id = 2;
foreach($data['languages'] as $language) {
	if($language['language_id'] != 1) {
		$language_id = $language['language_id'];
	}
}

$output = array();
$output["custom_module_module"] = array (
  1 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      $language_id => '',
      1 => '',
    ),
    'block_content' => 
    array (
      $language_id => '<p><br></p>',
      1 => '<p><br></p>',
    ),
    'html' => 
    array (
      $language_id => '<footer class="footer-container fluid-width-footer">
     <div class="container">
          <div class="footer-inner">
               <div class="footer-top">
                    <div class="footer-top-inner clearfix">
                         <div class="widget-block">
                              <div class="block-static row">
                                   <div class="block-wrap">
                                        <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                             <div class="home03-footer">
                                                  <div class="footer-logo"><img alt="" src="image/catalog/logo-logancee.png"></div>
                                                  <ul class="social-icons h4">
                                                       <li class="facebook"><a href="http://www.facebook.com/" target="_blank"><span>Facebook</span></a></li>
                                                       <li class="twitter"><a href="http://www.twitter.com/" target="_blank"><span>Twitter</span></a></li>
                                                       <li class="pinterest"><a href="https://www.pinterest.com/" target="_blank"><span>pinterest square</span></a></li>
                                                       <li class="gplus"><a href="https://plus.google.com/" target="_blank"><span>Google +</span></a></li>
                                                       <li class="instagram"><a href="https://instagram.com/" target="_blank"><span>Instagram</span></a></li>
                                                  </ul>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="footer-copyright">
                    <div class="row">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">© 2016 <a href="#">Logancee</a>. All Rights Reserved.</div>
                    </div>
               </div>
          </div
     </div>
</footer>',
      1 => '<footer class="footer-container fluid-width-footer">
     <div class="container">
          <div class="footer-inner">
               <div class="footer-top">
                    <div class="footer-top-inner clearfix">
                         <div class="widget-block">
                              <div class="block-static row">
                                   <div class="block-wrap">
                                        <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                             <div class="home03-footer">
                                                  <div class="footer-logo"><img alt="" src="image/catalog/logo-logancee.png"></div>
                                                  <ul class="social-icons h4">
                                                       <li class="facebook"><a href="http://www.facebook.com/" target="_blank"><span>Facebook</span></a></li>
                                                       <li class="twitter"><a href="http://www.twitter.com/" target="_blank"><span>Twitter</span></a></li>
                                                       <li class="pinterest"><a href="https://www.pinterest.com/" target="_blank"><span>pinterest square</span></a></li>
                                                       <li class="gplus"><a href="https://plus.google.com/" target="_blank"><span>Google +</span></a></li>
                                                       <li class="instagram"><a href="https://instagram.com/" target="_blank"><span>Instagram</span></a></li>
                                                  </ul>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="footer-copyright">
                    <div class="row">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">© 2016 <a href="#">Logancee</a>. All Rights Reserved.</div>
                    </div>
               </div>
          </div
     </div>
</footer>',
    ),
    'layout_id' => '99999',
    'position' => 'footer2',
    'status' => '1',
    'sort_order' => '',
  ),
  2 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      $language_id => '',
      1 => '',
    ),
    'block_content' => 
    array (
      $language_id => '<p><br></p>',
      1 => '<p><br></p>',
    ),
    'html' => 
    array (
      $language_id => '<div style="height: 42px"></div>',
      1 => '<div style="height: 42px"></div>',
    ),
    'layout_id' => '1',
    'position' => 'content_bottom',
    'status' => '1',
    'sort_order' => '35',
  ),
  3 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      $language_id => '',
      1 => '',
    ),
    'block_content' => 
    array (
      $language_id => '<p><br></p>',
      1 => '<p><br></p>',
    ),
    'html' => 
    array (
      $language_id => '<div class="row">
     <div class="main-top-inner">
          <div class="widget-block col-xs-12 col-sm-6 col-md-6 col-lg-6">
               <div class="block-static row">
                    <div class="block-wrap">
                         <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                              <div class="row">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="block-jw-text jw-text1">
                                             <div class="block-prent block-center">
                                                  <h2 class="title"><span>make a<br>different !</span></h2>
                                                  <div class="text desc std"><strong>Logance’s Jewelry</strong> brings unique new designs and sophisticated.<br>We always focus on creating trendy products and offers stunning scenery for users.</div>
                                                  <a class="btn-ctn h4" href="#"><span>Continue </span><span class="arrow_right">&nbsp;</span></a>
                                             </div>
                                        </div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="block-jw jw-5 bg-blue" style="background-image: url(\'image/catalog/block09_05.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="block-jw jw-6 bg-gray" style="background-image: url(\'image/catalog/block09_06.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="block-jw-text jw-text2">
                                             <div class="block-prent block-center">
                                                  <h2 class="title">about us</h2>
                                                  <div class="text desc std">We are a team of creative and<br>develop PSD Template...</div>
                                                       <a class="btn-ctn h4" href="#"><span>Continue </span><span class="arrow_right">&nbsp;</span></a>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
               
          <div class="widget-block col-xs-12 col-sm-6 col-md-6 col-lg-6">
               <div class="block-static row">
                    <div class="block-wrap">
                         <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                              <div class="row">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="block-jw jw-1 bg-orange" style="background-image: url(\'image/catalog/block09_01.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="block-jw jw-2 bg-brown" style="background-image: url(\'image/catalog/block09_02.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="block-jw-text jw-text2">
                                             <div class="block-prent block-center">
                                                  <div class="quote"><span class="icon_quotations">&nbsp;</span></div>
                                                  <div class="text desc std">A lot of the jewelry that I wear are fan gifts because they\'re so awesome and they give me great presents.</div>
                                                  <div class="user-name">taylor swift</div>
                                             </div>
                                        </div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="block-jw jw-3 bg-gray" style="background-image: url(\'image/catalog/block09_03.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="clear: both">
                                        <div class="block-jw jw-4 bg-green" style="background-image: url(\'image/catalog/block09_04.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>',
      1 => '<div class="row">
     <div class="main-top-inner">
          <div class="widget-block col-xs-12 col-sm-6 col-md-6 col-lg-6">
               <div class="block-static row">
                    <div class="block-wrap">
                         <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                              <div class="row">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="block-jw-text jw-text1">
                                             <div class="block-prent block-center">
                                                  <h2 class="title"><span>make a<br>different !</span></h2>
                                                  <div class="text desc std"><strong>Logance’s Jewelry</strong> brings unique new designs and sophisticated.<br>We always focus on creating trendy products and offers stunning scenery for users.</div>
                                                  <a class="btn-ctn h4" href="#"><span>Continue </span><span class="arrow_right">&nbsp;</span></a>
                                             </div>
                                        </div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="block-jw jw-5 bg-blue" style="background-image: url(\'image/catalog/block09_05.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="block-jw jw-6 bg-gray" style="background-image: url(\'image/catalog/block09_06.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="block-jw-text jw-text2">
                                             <div class="block-prent block-center">
                                                  <h2 class="title">about us</h2>
                                                  <div class="text desc std">We are a team of creative and<br>develop PSD Template...</div>
                                                       <a class="btn-ctn h4" href="#"><span>Continue </span><span class="arrow_right">&nbsp;</span></a>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
               
          <div class="widget-block col-xs-12 col-sm-6 col-md-6 col-lg-6">
               <div class="block-static row">
                    <div class="block-wrap">
                         <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                              <div class="row">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="block-jw jw-1 bg-orange" style="background-image: url(\'image/catalog/block09_01.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="block-jw jw-2 bg-brown" style="background-image: url(\'image/catalog/block09_02.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="block-jw-text jw-text2">
                                             <div class="block-prent block-center">
                                                  <div class="quote"><span class="icon_quotations">&nbsp;</span></div>
                                                  <div class="text desc std">A lot of the jewelry that I wear are fan gifts because they\'re so awesome and they give me great presents.</div>
                                                  <div class="user-name">taylor swift</div>
                                             </div>
                                        </div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="block-jw jw-3 bg-gray" style="background-image: url(\'image/catalog/block09_03.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                                        
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="clear: both">
                                        <div class="block-jw jw-4 bg-green" style="background-image: url(\'image/catalog/block09_04.jpg\');"><a class="btn-ex block-center hover-effect02" href="#"><span>Shop Now</span></a></div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>',
    ),
    'layout_id' => '1',
    'position' => 'preface_fullwidth',
    'status' => '1',
    'sort_order' => '1',
  ),
); 

$this->model_setting_setting->editSetting( "custom_module", $output );	

?>