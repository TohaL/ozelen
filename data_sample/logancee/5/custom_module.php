<?php 

$language_id = 2;
foreach($data['languages'] as $language) {
	if($language['language_id'] != 1) {
		$language_id = $language['language_id'];
	}
}

$output = array();
$output["custom_module_module"] = array (
  1 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      $language_id => '',
      1 => '',
    ),
    'block_content' => 
    array (
      $language_id => '<p><br></p>',
      1 => '<p><br></p>',
    ),
    'html' => 
    array (
      $language_id => '<div class="home04">
     <footer class="footer-container">
          <div class="container">
               <div class="footer-inner">
                    <div class="footer-bottom">
                         <div class="footer-bottom-inner clearfix">
                              <div class="widget-block " id="widget-67d15281e3c3804b1db6f9fcccc94bd4">
                                   <div class="block-static row">
                                        <div class="block-wrap">
                                             <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                                  <div class="row">
                                                       <div class="fc04 col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                                            <div class="copyright04">© 2016 <strong><a style="color: #000;" href="#">Logancee</a></strong>. All Rights Reserved.</div>
                                                       </div>
                                                       <div class="add04 hidden-xs hidden-sm hidden-md col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="address-footer">(0123) 456 7890<span class="space">|</span>123 New Design St, Melbourne, Australia<span class="space">|</span>youremail@domain.com</div>
                                                       </div>
                                                       <div class="sc04 hidden-xs col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                                            <div class="social">
                                                                 <ul class="social-icons">
                                                                      <li><span>Follow us: </span></li>
                                                                      <li class="twitter"><a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter"><span>Twitter</span></i></a></li>
                                                                      <li class="facebook"><a href="http://www.facebook.com/" target="_blank"><i class="fa fa-facebook"><span>Facebook</span></i></a></li>
                                                                      <li class="gplus"><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"><span>Google plus</span></i></a></li>
                                                                      <li class="instagram"><a href="https://instagram.com/" target="_blank"><i class="fa fa-instagram"><span>Instagram</span></i></a></li>
                                                                      <li class="pinterest"><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"><span>pinterest square</span></i></a></li>
                                                                 </ul>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </footer>
</div>',
      1 => '<div class="home04">
     <footer class="footer-container">
          <div class="container">
               <div class="footer-inner">
                    <div class="footer-bottom">
                         <div class="footer-bottom-inner clearfix">
                              <div class="widget-block " id="widget-67d15281e3c3804b1db6f9fcccc94bd4">
                                   <div class="block-static row">
                                        <div class="block-wrap">
                                             <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                                  <div class="row">
                                                       <div class="fc04 col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                                            <div class="copyright04">© 2016 <strong><a style="color: #000;" href="#">Logancee</a></strong>. All Rights Reserved.</div>
                                                       </div>
                                                       <div class="add04 hidden-xs hidden-sm hidden-md col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="address-footer">(0123) 456 7890<span class="space">|</span>123 New Design St, Melbourne, Australia<span class="space">|</span>youremail@domain.com</div>
                                                       </div>
                                                       <div class="sc04 hidden-xs col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                                            <div class="social">
                                                                 <ul class="social-icons">
                                                                      <li><span>Follow us: </span></li>
                                                                      <li class="twitter"><a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter"><span>Twitter</span></i></a></li>
                                                                      <li class="facebook"><a href="http://www.facebook.com/" target="_blank"><i class="fa fa-facebook"><span>Facebook</span></i></a></li>
                                                                      <li class="gplus"><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"><span>Google plus</span></i></a></li>
                                                                      <li class="instagram"><a href="https://instagram.com/" target="_blank"><i class="fa fa-instagram"><span>Instagram</span></i></a></li>
                                                                      <li class="pinterest"><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"><span>pinterest square</span></i></a></li>
                                                                 </ul>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </footer>
</div>',
    ),
    'layout_id' => '99999',
    'position' => 'footer2',
    'status' => '1',
    'sort_order' => '',
  ),
  2 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      $language_id => '',
      1 => '',
    ),
    'block_content' => 
    array (
      $language_id => '<p><br></p>',
      1 => '<p><br></p>',
    ),
    'html' => 
    array (
      $language_id => '<section class="main-container col1-layout">
     <div class="container">
          <div class="multiscroll">
               <div class="widget-block">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="multiscroll">
                                        <div class="ms-left">
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <h2 class="title"><span>welcome to<br />logancee store</span></h2>
                                                       <div class="text desc std">We&rsquo;re a team of creative and make amazing site in ecommerce from<br />Unite States. We love colour pastel, its make our design look so awesome.<br />Now ! come here and create fashion trending with us</div>
                                                  </div>
                                             </div>
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block2.jpg\');">&nbsp;</div>
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <div class="date-time"><span class="date">07</span>/ <span class="month">Sep</span></div>
                                                       <h3 class="title"><span>explore trending fashion in this week<br />with logan team</span></h3>
                                                       <div class="text desc std">Streets run one way and avenues run the other but with Arbor\'s Avenue <br />hoodie, you\'ll never lose your sense of direction and style. The full zip<br />toggle hoodie for men has a black body large zip front...</div>
                                                       <a class="btn-ctn h4" href="#"><span>Continue </span><span class="arrow_right">&nbsp;</span></a>
                                                  </div>
                                             </div>
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block4.jpg\');">&nbsp;</div>
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <div id="messages_product_view"></div>
                                                       <form action="index.php?route=information/contact" id="contactForm" method="post">
                                                            <div class="fieldset">
                                                                 <h2 class="title"><span>Contact Us</span></h2>
                                                                 <ul class="form-list">
                                                                      <li class="fields">
                                                                           <div class="field">
                                                                                <div class="input-box"><input name="name" id="name" title="Name" value="" class="input-text required-entry" type="text" placeholder="Your Name" /></div>
                                                                           </div>
                                                                                     
                                                                           <div class="field">
                                                                                <div class="input-box"><input name="email" id="email" title="Email" value="" class="input-text required-entry validate-email" type="text" placeholder="Your Email" /></div>
                                                                           </div>
                                                                      </li>
                                                                      <li class="wide">
                                                                           <div class="input-box"><textarea name="enquiry" id="comment" title="Message" class="required-entry input-text"  rows="5" placeholder="Message" ></textarea></div>
                                                                      </li>
                                                                 </ul>
                                                            </div>
                                                            <div class="buttons-set">
                                                                 <button type="submit" title="Send Message" class="button"><span><span>Send</span></span></button>
                                                            </div>
                                                       </form>
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="ms-right">
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block1.jpg\');">&nbsp;</div>
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <h2 class="title"><span>Leather bags<br />for men</span></h2>
                                                       <div class="text desc std">Collection leather bags by designer C-Knightz brought a new fashion trend<br />for men elegance and courtesy. Combined with material from the leather luxury<br />category and also meticulous in every detail of the product, this collection<br />deserves a perfect choice for fashionable gentlemen.</div>
                                                       <a class="btn-ex button" href="#"><span>Explore Now</span></a>
                                                  </div>
                                             </div>
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block3.jpg\');">&nbsp;</div>
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <div class="quote"><span class="icon_quotations">&nbsp;</span></div>
                                                       <div class="text desc std">I really enjoyed shopping at Logancee store<br />because here clothing very beautiful, so cool and<br />very appropriate price.</div>
                                                       <div class="user-position"><span class="name h4">C-knightz art -</span> <span class="position">Fashion Stylist</span></div>
                                                  </div>
                                             </div>
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block5.jpg\');">&nbsp;</div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>


<link rel="stylesheet" type="text/css" href="catalog/view/theme/logancee/css/jquery.multiscroll.css" />
<script type="text/javascript" src="catalog/view/theme/logancee/js/jquery.multiscroll.min.js"></script>


<script type="text/javascript">
     $(document).ready(function() {
          $("body").addClass("activemultiscroll");
          if($(\'#multiscroll\').length){
               $(\'#multiscroll\').multiscroll({
                    onLeave:function(index,newIndex,direction){
                         if(direction==\'down\') {
                              $(\'#multiscroll .ms-section\').removeClass(\'moveUp moveDown\');
                              $(\'#multiscroll .ms-section\').eq(newIndex-1).addClass(\'moveUp\');
                         } else if(direction==\'up\'){
                              $(\'#multiscroll .ms-section\').removeClass(\'moveUp moveDown\');
                              $(\'#multiscroll .ms-section\').eq(newIndex-1).addClass(\'moveDown\');
                         }
                    }
               });
          }
     });
</script>',
      1 => '<section class="main-container col1-layout">
     <div class="container">
          <div class="multiscroll">
               <div class="widget-block">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="multiscroll">
                                        <div class="ms-left">
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <h2 class="title"><span>welcome to<br />logancee store</span></h2>
                                                       <div class="text desc std">We&rsquo;re a team of creative and make amazing site in ecommerce from<br />Unite States. We love colour pastel, its make our design look so awesome.<br />Now ! come here and create fashion trending with us</div>
                                                  </div>
                                             </div>
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block2.jpg\');">&nbsp;</div>
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <div class="date-time"><span class="date">07</span>/ <span class="month">Sep</span></div>
                                                       <h3 class="title"><span>explore trending fashion in this week<br />with logan team</span></h3>
                                                       <div class="text desc std">Streets run one way and avenues run the other but with Arbor\'s Avenue <br />hoodie, you\'ll never lose your sense of direction and style. The full zip<br />toggle hoodie for men has a black body large zip front...</div>
                                                       <a class="btn-ctn h4" href="#"><span>Continue </span><span class="arrow_right">&nbsp;</span></a>
                                                  </div>
                                             </div>
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block4.jpg\');">&nbsp;</div>
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <div id="messages_product_view"></div>
                                                       <form action="index.php?route=information/contact" id="contactForm" method="post">
                                                            <div class="fieldset">
                                                                 <h2 class="title"><span>Contact Us</span></h2>
                                                                 <ul class="form-list">
                                                                      <li class="fields">
                                                                           <div class="field">
                                                                                <div class="input-box"><input name="name" id="name" title="Name" value="" class="input-text required-entry" type="text" placeholder="Your Name" /></div>
                                                                           </div>
                                                                                     
                                                                           <div class="field">
                                                                                <div class="input-box"><input name="email" id="email" title="Email" value="" class="input-text required-entry validate-email" type="text" placeholder="Your Email" /></div>
                                                                           </div>
                                                                      </li>
                                                                      <li class="wide">
                                                                           <div class="input-box"><textarea name="enquiry" id="comment" title="Message" class="required-entry input-text"  rows="5" placeholder="Message" ></textarea></div>
                                                                      </li>
                                                                 </ul>
                                                            </div>
                                                            <div class="buttons-set">
                                                                 <button type="submit" title="Send Message" class="button"><span><span>Send</span></span></button>
                                                            </div>
                                                       </form>
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="ms-right">
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block1.jpg\');">&nbsp;</div>
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <h2 class="title"><span>Leather bags<br />for men</span></h2>
                                                       <div class="text desc std">Collection leather bags by designer C-Knightz brought a new fashion trend<br />for men elegance and courtesy. Combined with material from the leather luxury<br />category and also meticulous in every detail of the product, this collection<br />deserves a perfect choice for fashionable gentlemen.</div>
                                                       <a class="btn-ex button" href="#"><span>Explore Now</span></a>
                                                  </div>
                                             </div>
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block3.jpg\');">&nbsp;</div>
                                             <div class="ms-section">
                                                  <div class="block-prent block-center">
                                                       <div class="quote"><span class="icon_quotations">&nbsp;</span></div>
                                                       <div class="text desc std">I really enjoyed shopping at Logancee store<br />because here clothing very beautiful, so cool and<br />very appropriate price.</div>
                                                       <div class="user-position"><span class="name h4">C-knightz art -</span> <span class="position">Fashion Stylist</span></div>
                                                  </div>
                                             </div>
                                             <div class="ms-section" style="background-image: url(\'image/catalog/img-block5.jpg\');">&nbsp;</div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>


<link rel="stylesheet" type="text/css" href="catalog/view/theme/logancee/css/jquery.multiscroll.css" />
<script type="text/javascript" src="catalog/view/theme/logancee/js/jquery.multiscroll.min.js"></script>


<script type="text/javascript">
     $(document).ready(function() {
          $("body").addClass("activemultiscroll");
          if($(\'#multiscroll\').length){
               $(\'#multiscroll\').multiscroll({
                    onLeave:function(index,newIndex,direction){
                         if(direction==\'down\') {
                              $(\'#multiscroll .ms-section\').removeClass(\'moveUp moveDown\');
                              $(\'#multiscroll .ms-section\').eq(newIndex-1).addClass(\'moveUp\');
                         } else if(direction==\'up\'){
                              $(\'#multiscroll .ms-section\').removeClass(\'moveUp moveDown\');
                              $(\'#multiscroll .ms-section\').eq(newIndex-1).addClass(\'moveDown\');
                         }
                    }
               });
          }
     });
</script>',
    ),
    'layout_id' => '1',
    'position' => 'preface_fullwidth',
    'status' => '1',
    'sort_order' => '',
  ),
); 

$this->model_setting_setting->editSetting( "custom_module", $output );	

?>