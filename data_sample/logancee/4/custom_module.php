<?php 

$language_id = 2;
foreach($data['languages'] as $language) {
	if($language['language_id'] != 1) {
		$language_id = $language['language_id'];
	}
}

$output = array();
$output["custom_module_module"] = array (
  1 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      $language_id => '',
      1 => '',
    ),
    'block_content' => 
    array (
      $language_id => '<p><br></p>',
      1 => '<p><br></p>',
    ),
    'html' => 
    array (
      $language_id => '<div class="home04">
     <footer class="footer-container">
          <div class="container">
               <div class="footer-inner">
                    <div class="footer-bottom">
                         <div class="footer-bottom-inner clearfix">
                              <div class="widget-block " id="widget-67d15281e3c3804b1db6f9fcccc94bd4">
                                   <div class="block-static row">
                                        <div class="block-wrap">
                                             <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                                  <div class="row">
                                                       <div class="fc04 col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                                            <div class="copyright04">© 2016 <strong><a style="color: #000;" href="#">Logancee</a></strong>. All Rights Reserved.</div>
                                                       </div>
                                                       <div class="add04 hidden-xs hidden-sm hidden-md col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="address-footer">(0123) 456 7890<span class="space">|</span>123 New Design St, Melbourne, Australia<span class="space">|</span>youremail@domain.com</div>
                                                       </div>
                                                       <div class="sc04 hidden-xs col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                                            <div class="social">
                                                                 <ul class="social-icons">
                                                                      <li><span>Follow us: </span></li>
                                                                      <li class="twitter"><a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter"><span>Twitter</span></i></a></li>
                                                                      <li class="facebook"><a href="http://www.facebook.com/" target="_blank"><i class="fa fa-facebook"><span>Facebook</span></i></a></li>
                                                                      <li class="gplus"><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"><span>Google plus</span></i></a></li>
                                                                      <li class="instagram"><a href="https://instagram.com/" target="_blank"><i class="fa fa-instagram"><span>Instagram</span></i></a></li>
                                                                      <li class="pinterest"><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"><span>pinterest square</span></i></a></li>
                                                                 </ul>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </footer>
</div>',
      1 => '<div class="home04">
     <footer class="footer-container">
          <div class="container">
               <div class="footer-inner">
                    <div class="footer-bottom">
                         <div class="footer-bottom-inner clearfix">
                              <div class="widget-block " id="widget-67d15281e3c3804b1db6f9fcccc94bd4">
                                   <div class="block-static row">
                                        <div class="block-wrap">
                                             <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                                  <div class="row">
                                                       <div class="fc04 col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                                            <div class="copyright04">© 2016 <strong><a style="color: #000;" href="#">Logancee</a></strong>. All Rights Reserved.</div>
                                                       </div>
                                                       <div class="add04 hidden-xs hidden-sm hidden-md col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="address-footer">(0123) 456 7890<span class="space">|</span>123 New Design St, Melbourne, Australia<span class="space">|</span>youremail@domain.com</div>
                                                       </div>
                                                       <div class="sc04 hidden-xs col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                                            <div class="social">
                                                                 <ul class="social-icons">
                                                                      <li><span>Follow us: </span></li>
                                                                      <li class="twitter"><a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter"><span>Twitter</span></i></a></li>
                                                                      <li class="facebook"><a href="http://www.facebook.com/" target="_blank"><i class="fa fa-facebook"><span>Facebook</span></i></a></li>
                                                                      <li class="gplus"><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"><span>Google plus</span></i></a></li>
                                                                      <li class="instagram"><a href="https://instagram.com/" target="_blank"><i class="fa fa-instagram"><span>Instagram</span></i></a></li>
                                                                      <li class="pinterest"><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"><span>pinterest square</span></i></a></li>
                                                                 </ul>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </footer>
</div>',
    ),
    'layout_id' => '99999',
    'position' => 'footer2',
    'status' => '1',
    'sort_order' => '',
  ),
  2 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      $language_id => '',
      1 => '',
    ),
    'block_content' => 
    array (
      $language_id => '<p><br></p>',
      1 => '<p><br></p>',
    ),
    'html' => 
    array (
      $language_id => '          <div id="fullpage">
               <div class="widget-block" id="widget1" style="background-image: url(image/catalog/bg-parallax1.jpg);">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section1">
                                        <div class="intro">
                                             <div class="text-small">Fashion trending 2015</div>
                                             <h2 class="title">Impression with men tattoo</h2>
                                             <a class="btn-ex hover-effect07" href="#"><span>Explore Now</span></a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="widget-block " id="widget2" style="background-image: url(image/catalog/bg-parallax2.jpg);">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section2">
                                        <div class="intro">
                                             <img class="img-responsive" src="image/catalog/logo-parallax.png" alt="" />
                                             <div class="text-normal">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut<br />labore et dolore magna aliqua.Praesent volutpat ut nisl in hendrerit.</div>
                                             <a class="btn-ex hover-effect02" href="#"><span>Shop Now</span></a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="widget-block " id="widget3" style="background-image: url(image/catalog/bg-parallax3.jpg);">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section3">
                                        <div class="intro">
                                             <h2 class="title">Super sale</h2>
                                             <div class="text-normal">In anniversay 5 years of LOGANCEE store. We&rsquo;re glad notice will sale off 30% for all<br />product on our store. Wow ! Come here.</div>
                                             <a class="btn-ex hover-effect02" href="#"><span>View Now</span></a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="widget-block " id="widget4">
                    <div class="video-frame"><iframe title="YouTube video player" src="https://www.youtube.com/embed/vgs1J11H3GM?autoplay=1&autohide=1&controls=0&loop=1&wmode=transparent&html5=1&rel=0&showinfo=0&modestbranding=0&playlist=vgs1J11H3GM&enablejsapi=1&origin=http%3A%2F%2Flogancee04.typostores.com" width="1980" height="1113" allowfullscreen="allowfullscreen"></iframe></div>
                    
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section4">                                        
                                        <div class="intro">
                                             <h2 class="title">Our classic collections</h2>
                                             <div class="text-normal">We beleive that you will so want to know<br />what things make perfection for our classic collections</div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="widget-block " id="widget5" style="background-image: url(image/catalog/bg-parallax5.jpg);">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section5">
                                        <div class="intro">
                                             <div class="text-normal">I love LoganCee fashion, here have everything that<br />i need, its so impression and style, materials was combined so good<br />and i alway choive Logancee for my style.</div>
                                             <div class="name">Altom Kaverbin</div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/logancee/css/jquery.fullPage.css" />
<script type="text/javascript" src="catalog/view/theme/logancee/js/jquery.fullPage.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
     $(\'body\').addClass(\'home04\');
});

jQuery(document).ready(function($){
     if($(\'#fullpage\').length){
          $(\'#fullpage > div\').addClass(\'section moveDown\');
          $(\'#fullpage\').fullpage({
               navigation:true,
               navigationPosition:\'right\',
               sectionSelector:\'.section\'
          });
     }
});
</script>',
      1 => '          <div id="fullpage">
               <div class="widget-block" id="widget1" style="background-image: url(image/catalog/bg-parallax1.jpg);">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section1">
                                        <div class="intro">
                                             <div class="text-small">Fashion trending 2015</div>
                                             <h2 class="title">Impression with men tattoo</h2>
                                             <a class="btn-ex hover-effect07" href="#"><span>Explore Now</span></a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="widget-block " id="widget2" style="background-image: url(image/catalog/bg-parallax2.jpg);">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section2">
                                        <div class="intro">
                                             <img class="img-responsive" src="image/catalog/logo-parallax.png" alt="" />
                                             <div class="text-normal">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut<br />labore et dolore magna aliqua.Praesent volutpat ut nisl in hendrerit.</div>
                                             <a class="btn-ex hover-effect02" href="#"><span>Shop Now</span></a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="widget-block " id="widget3" style="background-image: url(image/catalog/bg-parallax3.jpg);">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section3">
                                        <div class="intro">
                                             <h2 class="title">Super sale</h2>
                                             <div class="text-normal">In anniversay 5 years of LOGANCEE store. We&rsquo;re glad notice will sale off 30% for all<br />product on our store. Wow ! Come here.</div>
                                             <a class="btn-ex hover-effect02" href="#"><span>View Now</span></a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="widget-block " id="widget4">
                    <div class="video-frame"><iframe title="YouTube video player" src="https://www.youtube.com/embed/vgs1J11H3GM?autoplay=1&autohide=1&controls=0&loop=1&wmode=transparent&html5=1&rel=0&showinfo=0&modestbranding=0&playlist=vgs1J11H3GM&enablejsapi=1&origin=http%3A%2F%2Flogancee04.typostores.com" width="1980" height="1113" allowfullscreen="allowfullscreen"></iframe></div>
                    
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section4">                                        
                                        <div class="intro">
                                             <h2 class="title">Our classic collections</h2>
                                             <div class="text-normal">We beleive that you will so want to know<br />what things make perfection for our classic collections</div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               
               <div class="widget-block " id="widget5" style="background-image: url(image/catalog/bg-parallax5.jpg);">
                    <div class="block-static row">
                         <div class="block-wrap">
                              <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12  ">
                                   <div id="section5">
                                        <div class="intro">
                                             <div class="text-normal">I love LoganCee fashion, here have everything that<br />i need, its so impression and style, materials was combined so good<br />and i alway choive Logancee for my style.</div>
                                             <div class="name">Altom Kaverbin</div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/logancee/css/jquery.fullPage.css" />
<script type="text/javascript" src="catalog/view/theme/logancee/js/jquery.fullPage.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
     $(\'body\').addClass(\'home04\');
});

jQuery(document).ready(function($){
     if($(\'#fullpage\').length){
          $(\'#fullpage > div\').addClass(\'section moveDown\');
          $(\'#fullpage\').fullpage({
               navigation:true,
               navigationPosition:\'right\',
               sectionSelector:\'.section\'
          });
     }
});
</script>',
    ),
    'layout_id' => '1',
    'position' => 'preface_fullwidth',
    'status' => '1',
    'sort_order' => '',
  ),
); 

$this->model_setting_setting->editSetting( "custom_module", $output );	

?>