<?php 

$language_id = 2;
foreach($data['languages'] as $language) {
	if($language['language_id'] != 1) {
		$language_id = $language['language_id'];
	}
}

$output = array();
$output["custom_module_module"] = array (
  1 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '<p><br></p>',
      $language_id => '<p><br></p>',
    ),
    'html' => 
    array (
      1 => '<div class="main-slide-sidebar">
     <div class="row">
          <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
               <div class="block-sidebar animate-fadeIn active hover-effect01" style="height: 328px; background: url(image/catalog/block-slide-sidebar.jpg) center center no-repeat; background-size: cover;">
                    <h2 class="title"><a href="#">INK<br>glasses</a></h2>
               </div>
          </div>
          
          <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
               <div class="block-sidebar animate-fadeIn active" style="height: 219px; background-color: #000;">
                    <div class="block-promo">
                         <div class="title"><h2>Super Sale!</h2></div>
                         <div class="text">With 1000+ New Exclusive<br>Makedowns Products</div>
                         <a class="btn-link hover-effect07" href="#"><span>View Now</span></a>
                    </div>
               </div>
          </div>
     </div>
</div>',
      $language_id => '<div class="main-slide-sidebar">
     <div class="row">
          <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
               <div class="block-sidebar animate-fadeIn active hover-effect01" style="height: 328px; background: url(image/catalog/block-slide-sidebar.jpg) center center no-repeat; background-size: cover;">
                    <h2 class="title"><a href="#">INK<br>glasses</a></h2>
               </div>
          </div>
          
          <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
               <div class="block-sidebar animate-fadeIn active" style="height: 219px; background-color: #000;">
                    <div class="block-promo">
                         <div class="title"><h2>Super Sale!</h2></div>
                         <div class="text">With 1000+ New Exclusive<br>Makedowns Products</div>
                         <a class="btn-link hover-effect07" href="#"><span>View Now</span></a>
                    </div>
               </div>
          </div>
     </div>
</div>',
    ),
    'layout_id' => '1',
    'position' => 'preface_right',
    'status' => '1',
    'sort_order' => '',
  ),
  2 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '<p><br></p>',
      $language_id => '<p><br></p>',
    ),
    'html' => 
    array (
      1 => '<div class="block-static row">
     <div class="block-wrap">
          <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12">
               <div class="mini-block-top clearfix">
                    <div class="row">
                         <div class="col-tiny col-xs-6 col-sm-5 col-md-5 col-lg-5">
                              <div class="static-collection">
                                   <h2 class="title">Autumn Collection</h2>
                                   <div class="text-wrap" style="display: block; width: 100%; height: 672px; background: url(image/catalog/static-collection.jpg) center center no-repeat; background-size: cover;">
                                        <div class="info">
                                             <h3>Inspiraton from Cknightz</h3>
                                             <div class="text">Discover the best collection for autumn of Stylist Cknightz Art. With so charming, modern and passionate, you certainly love this collection.</div>
                                             <a class="btn-ex hover-effect02" href="#"><span>Explore Now</span></a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         
                         <div class="col-tiny col-xs-6 col-sm-7 col-md-7 col-lg-7">
                              <div class="static-collection">
                                   <h2 class="title">Denim jean - trend of jean 2015</h2>
                                   <a class="links" style="display: block; width: 100%; height: 211px; background: url(\'image/catalog/static-collection2.jpg\') center center no-repeat; background-size: cover;" href="#"><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum illum, aut, sunt molestias tempora amet!</span></a>
                              </div>
                         </div>
                         
                         <div class="col-tiny col-xs-6 col-sm-7 col-md-7 col-lg-7">
                              <div class="static-collection">
                                   <h2 class="title">Fashion Trens 2015 - Hipster</h2>
                                   <iframe style="width: 100%; border: none;" src="https://www.youtube.com/embed/Gl0_pwlYC1Y?rel=0&amp;showinfo=0&amp;controls=0&amp;autohide=1" height="377" width="670"></iframe>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>',
      $language_id => '<div class="block-static row">
     <div class="block-wrap">
          <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12">
               <div class="mini-block-top clearfix">
                    <div class="row">
                         <div class="col-tiny col-xs-6 col-sm-5 col-md-5 col-lg-5">
                              <div class="static-collection">
                                   <h2 class="title">Autumn Collection</h2>
                                   <div class="text-wrap" style="display: block; width: 100%; height: 672px; background: url(image/catalog/static-collection.jpg) center center no-repeat; background-size: cover;">
                                        <div class="info">
                                             <h3>Inspiraton from Cknightz</h3>
                                             <div class="text">Discover the best collection for autumn of Stylist Cknightz Art. With so charming, modern and passionate, you certainly love this collection.</div>
                                             <a class="btn-ex hover-effect02" href="#"><span>Explore Now</span></a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         
                         <div class="col-tiny col-xs-6 col-sm-7 col-md-7 col-lg-7">
                              <div class="static-collection">
                                   <h2 class="title">Denim jean - trend of jean 2015</h2>
                                   <a class="links" style="display: block; width: 100%; height: 211px; background: url(\'image/catalog/static-collection2.jpg\') center center no-repeat; background-size: cover;" href="#"><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum illum, aut, sunt molestias tempora amet!</span></a>
                              </div>
                         </div>
                         
                         <div class="col-tiny col-xs-6 col-sm-7 col-md-7 col-lg-7">
                              <div class="static-collection">
                                   <h2 class="title">Fashion Trens 2015 - Hipster</h2>
                                   <iframe style="width: 100%; border: none;" src="https://www.youtube.com/embed/Gl0_pwlYC1Y?rel=0&amp;showinfo=0&amp;controls=0&amp;autohide=1" height="377" width="670"></iframe>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>',
    ),
    'layout_id' => '1',
    'position' => 'preface_fullwidth',
    'status' => '1',
    'sort_order' => '0',
  ),
  3 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '<p><br></p>',
      $language_id => '<p><br></p>',
    ),
    'html' => 
    array (
      1 => '						          <div class="social-topbar col-xs-10 col-sm-3 col-md-4 col-lg-3">
						               <div class="social">
						                    <ul class="social-icons small light">
						                         <li class="twitter"><a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter"><span>Twitter</span></i></a></li>
						                         <li class="facebook"><a href="http://www.facebook.com/" target="_blank"><i class="fa fa-facebook"><span>Facebook</span></i></a></li>
						                         <li class="gplus"><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"><span>Google plus</span></i></a></li>
						                         <li class="instagram"><a href="https://instagram.com/" target="_blank"><i class="fa fa-instagram"><span>Instagram</span></i></a></li>
						                         <li class="pinterest"><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"><span>pinterest square</span></i></a></li>
						                    </ul>
						               </div>
						          </div>',
      $language_id => '						          <div class="social-topbar col-xs-10 col-sm-3 col-md-4 col-lg-3">
						               <div class="social">
						                    <ul class="social-icons small light">
						                         <li class="twitter"><a href="http://www.twitter.com/" target="_blank"><i class="fa fa-twitter"><span>Twitter</span></i></a></li>
						                         <li class="facebook"><a href="http://www.facebook.com/" target="_blank"><i class="fa fa-facebook"><span>Facebook</span></i></a></li>
						                         <li class="gplus"><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"><span>Google plus</span></i></a></li>
						                         <li class="instagram"><a href="https://instagram.com/" target="_blank"><i class="fa fa-instagram"><span>Instagram</span></i></a></li>
						                         <li class="pinterest"><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"><span>pinterest square</span></i></a></li>
						                    </ul>
						               </div>
						          </div>',
    ),
    'layout_id' => '99999',
    'position' => 'top_block',
    'status' => '1',
    'sort_order' => '',
  ),
); 

$this->model_setting_setting->editSetting( "custom_module", $output );	

?>