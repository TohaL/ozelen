<?php
class ModelToolImage extends Model {
	public function resize($filename, $width, $height) {
		if (!is_file(DIR_IMAGE . $filename)) {
			return;
		}

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$old_image = $filename;
		$new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
			$path = '';

			$directories = explode('/', dirname(str_replace('../', '', $new_image)));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}
			}

			list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image(DIR_IMAGE . $old_image);
				$image->resize($width, $height);
				$image->save(DIR_IMAGE . $new_image);
			} else {
				copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
			}
		}

		if ($this->request->server['HTTPS']) {
			return $this->config->get('config_ssl') . 'image/' . $new_image;
		} else {
			return $this->config->get('config_url') . 'image/' . $new_image;
		}
	}

	public function myResize($filename, $width, $height, $is_crop = true, $type = "") {

			 if (!file_exists(DIR_IMAGE . $filename) || !is_file(DIR_IMAGE . $filename)) {
					 return;
			 }

			 $info = pathinfo($filename);

			 $extension = $info['extension'];

			 $old_image = $filename;

			 $image = new Image(DIR_IMAGE . $old_image);
			 list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

			 if ($is_crop){
			 	$scaleW = $width_orig/$width;
			 	$scaleH = $height_orig/$height;


			 	if ($scaleH > $scaleW) {
			 		 $_height = $height * $scaleW;

			 		 $top_x = 0;
			 		 $top_y = ($height_orig - $_height) / 2;

			 		 $bottom_x = $width_orig;
			 		 $bottom_y = $top_y + $_height;

			 		 $image->crop($top_x, $top_y, $bottom_x, $bottom_y);
			  } elseif ($scaleH < $scaleW) {
			 		 $_width = $width * $scaleH;

			 		 $top_x = ($width_orig - $_width) / 2;
			 		 $top_y = 0;

			 		 $bottom_x = $top_x + $_width;
			 		 $bottom_y = $height_orig;

			 		 $image->crop($top_x, $top_y, $bottom_x, $bottom_y);
			  }
			 } else {
			  if ($width_orig > $height_orig){
			 		$height = round($height_orig*$width/$width_orig);
			  } else {
			 		$width = round($width_orig*$height/$height_orig);
			  }
			 }


			 $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . $type .'.' . $extension;

			 if (!file_exists(DIR_IMAGE . $new_image) || (filemtime(DIR_IMAGE . $old_image) > filemtime(DIR_IMAGE . $new_image))) {
					 $path = '';

					 $directories = explode('/', dirname(str_replace('../', '', $new_image)));

					 foreach ($directories as $directory) {
							 $path = $path . '/' . $directory;

							 if (!file_exists(DIR_IMAGE . $path)) {
									 @mkdir(DIR_IMAGE . $path, 0777);
							 }
					 }

					 list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

					 if ($width_orig != $width || $height_orig != $height) {
						 $image = new Image(DIR_IMAGE . $old_image);
						 if ($is_crop){
						  $scaleW = $width_orig/$width;
						  $scaleH = $height_orig/$height;


						  if ($scaleH > $scaleW) {
						 		$_height = $height * $scaleW;

						 		$top_x = 0;
						 		$top_y = ($height_orig - $_height) / 2;

						 		$bottom_x = $width_orig;
						 		$bottom_y = $top_y + $_height;

						 		$image->crop($top_x, $top_y, $bottom_x, $bottom_y);
						  } elseif ($scaleH < $scaleW) {
						 		$_width = $width * $scaleH;

						 		$top_x = ($width_orig - $_width) / 2;
						 		$top_y = 0;

						 		$bottom_x = $top_x + $_width;
						 		$bottom_y = $height_orig;

						 		$image->crop($top_x, $top_y, $bottom_x, $bottom_y);
						  }
						 } else {
						  if ($width_orig > $height_orig){
						 	 $height = round($height_orig*$width/$width_orig);
						  } else {
						 	 $width = round($width_orig*$height/$height_orig);
						  }
						 }


						 $image->resize($width, $height, $type);
						 $image->save(DIR_IMAGE . $new_image);
					 } else {
							 copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
					 }

			 }

			 if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
					 return $this->config->get('config_ssl') . 'image/' . $new_image;
			 } else {
					 return $this->config->get('config_url') . 'image/' . $new_image;
			 }
	 }
}
