<?php
class ModelCatalogSimpleGallery extends Model {
	public function getSimpleGallery($simple_gallery_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "simple_gallery p WHERE p.simple_gallery_id = '" . (int)$simple_gallery_id . "' ");

		return $query->row;
	}

	public function getIdSimpleGalleryByModule($module_id) {
		$query = $this->db->query("SELECT DISTINCT simple_gallery_id FROM " . DB_PREFIX . "simple_gallery p WHERE p.module_id = '" . (int)$module_id. "'");

		if (!empty($query->row)){
			return $query->row['simple_gallery_id'];
		} else {
			return false;
		}
	}

	public function getSimpleGalleries($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "simple_gallery p ";

		$sql .= " GROUP BY p.simple_gallery_id";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getSimpleGalleryImages($simple_gallery_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "simple_gallery_image WHERE simple_gallery_id = '" . (int)$simple_gallery_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getTotalSimpleGallerys($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.simple_gallery_id) AS total FROM " . DB_PREFIX . "simple_gallery p";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

}
