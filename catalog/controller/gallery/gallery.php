<?php
class ControllerGalleryGallery extends Controller {
	private $error = array();

	public function view() {
		$this->load->language('gallery/gallery');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);


//		$this->load->model('catalog/category');


		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}



		$module_id = $this->request->get['gallery_id'];
		$this->load->model('extension/module');
		$this->load->model('catalog/simple_gallery');
		$this->load->model('tool/image');

		$data['language_id'] = $this->config->get('config_language_id');

//		echo 'module_id: '.$module_id.'<br>';
		$gallery = $this->model_extension_module->getModule($module_id);


		if (isset($gallery['simple_gallery_image'])){
			$data['images'] = $gallery['simple_gallery_image'];
		} else {
			$data['images'] = array();
		}
		array_unshift($data['images'],array('image' => $gallery['image'], 'sort_order'=>0)) ;

		foreach ($data['images'] as &$image){
//			echo $image['image']."<br>";
			$image['thumb'] = $this->model_tool_image->myResize($image['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
		}
//		die;
		
//		echo "<pre>";
//		print_r($gallery);
//		print_r($data);
//		die;

//		$data['continue'] = $this->url->link('common/home');
		$data['heading_title'] = $gallery['name'];

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');



		$this->response->setOutput($this->load->view('gallery/gallery', $data));

	}

	public function index() {
		$this->load->language('gallery/gallery');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['continue'] = $this->url->link('common/home');
		$data['heading_title'] = $this->language->get('heading_title');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

//		$this->load->model('catalog/category');



		

		$url = '';


		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$this->response->setOutput($this->load->view('gallery/gallery_list', $data));

	}
//
//	public function review() {
//		$this->load->language('gallery/gallery');
//
//		$this->load->model('catalog/review');
//
//		$data['text_no_reviews'] = $this->language->get('text_no_reviews');
//
//		if (isset($this->request->get['page'])) {
//			$page = $this->request->get['page'];
//		} else {
//			$page = 1;
//		}
//
//		$data['reviews'] = array();
//
//		$review_total = $this->model_catalog_review->getTotalReviewsBygalleryId($this->request->get['gallery_id']);
//
//		$results = $this->model_catalog_review->getReviewsBygalleryId($this->request->get['gallery_id'], ($page - 1) * 5, 5);
//
//		foreach ($results as $result) {
//			$data['reviews'][] = array(
//				'author'     => $result['author'],
//				'text'       => nl2br($result['text']),
//				'rating'     => (int)$result['rating'],
//				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
//			);
//		}
//
//		$pagination = new Pagination();
//		$pagination->total = $review_total;
//		$pagination->page = $page;
//		$pagination->limit = 5;
//		$pagination->url = $this->url->link('gallery/gallery/review', 'gallery_id=' . $this->request->get['gallery_id'] . '&page={page}');
//
//		$data['pagination'] = $pagination->render();
//
//		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));
//
//		$this->response->setOutput($this->load->view('gallery/review', $data));
//	}

}
