<?php  
class ControllerModuleSimpleGallery extends Controller {
	public function index($setting) {
		if(isset($setting['module_id'])) {
			$this->load->language('module/simpleGallery');
			$this->load->model('extension/module');
			$this->load->model('catalog/simple_gallery');
			$data = $this->model_extension_module->getModule($setting['module_id']);
			$data['language_id'] = $this->config->get('config_language_id');
			$data['href'] = $this->url->link('gallery/gallery/view', 'gallery_id=' . $setting['module_id']);
			$this->load->model('tool/image');

//			print_r($setting['image']);
//			die;
			$data['thumb'] = $this->model_tool_image->myResize($setting['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));


//			echo "<pre>";
//			print_r($setting);
//			print_r($data);
//			if ($setting['module_id'] != 46)
//			die;

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/simpleGallery.tpl')) {
				return $this->load->view('module/simpleGallery', $data);
			} else {
				return $this->load->view('module/simpleGallery', $data);
			}
		}
	}
}?>