<?php
class ControllerModuleCategory extends Controller {
	public function index() {
		$this->load->language('module/category');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$childrens = array();
		$this->children(0,0,$childrens);
		$data['categories'] = $childrens;
		$data['parts'] = $parts;

		return $this->load->view('module/category', $data);
	}


	private function children($parent_id, $parent_path, &$children_data){
		$children = $this->model_catalog_category->getCategories($parent_id);
		foreach($children as $key=>$child) {

			$filter_data = array(
				'filter_category_id'  => $child['category_id'],
				'filter_sub_category' => true
			);

			$children_data[$child['category_id']] = array(
				'parent_path' => $parent_path,
				'level' => sizeof($children)-1,
				'category_id' => $child['category_id'],
				'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
				'href' => $this->url->link('product/category', 'path=' . $parent_path. '_' . $child['category_id'])
			);
		}

		foreach($children as $key=>$child) {
			$next_children = $this->model_catalog_category->getCategories($child['category_id']);
			if (!empty($next_children)){
				$this->children($child['category_id'], $parent_path.'_'.$child['category_id'], $children_data[$child['category_id']]['children']);
			}
		}
	}


}