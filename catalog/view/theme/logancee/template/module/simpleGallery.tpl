
<?php
//echo $thumb;
?>
<div class="col-sm-4 col-xs-6">

    <!-- Product -->
    <div class="product clearfix product-hover">
        <div class="left">
            <div class="image ">
                <a href="<?php echo $href; ?>">
                    <img src="<?php echo $thumb; ?>" alt="<?php echo $module_description[$language_id]['title']; ?>" class="zoom-image-effect">
                </a>
            </div>
        </div>
        <div class="right">
            <div class="name"><a href="<?php echo $href; ?>"><?php echo $module_description[$language_id]['title']; ?></a></div>
        </div>
    </div>
</div>
