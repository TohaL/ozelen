<?php

if (!function_exists('printChildCategory')){
    function printChildCategory($categories, &$html, $category_id, &$level, $parts){
        $html .= '<div id="category'.$categories['category_id'].'" class="panel-collapse collapse fuck';
        if (in_array($categories['category_id'],$parts)) $html .= ' in';
        $html .= '"><ul>';
        if (!empty($categories['children'])){
            $level ++;
            foreach ($categories['children'] as $key=>$category){
                $html .= '<li style="position: relative; padding-left: 15px">';
                if ($category['category_id'] == $category_id) {
                    $html .= '<a href="'.$category['href'].'" data-level="'.$level.'" class="active">'
                        .$category['name']
                    .'</a>';
                } else {
                    $html .= '<a href="'.$category['href'].'" data-level="'.$level.'">'.$category['name'].'</a>';
                }
                if (!empty($category['children'])){
                    $html .= '<span class="head"><a style="float:right;padding-right:5px" class="accordion-toggle';
                    if (in_array($category['category_id'],$parts)) {
                        $html .= ' collapse';
                    } else {
                        $html .= ' collapsed';
                    };
                    $html .='"';
                    $html .= 'data-toggle="collapse" href="#category'.$category['category_id'].'"><span class="plus">+</span><span 
                    class="minus">-</span></a></span>';
                }
                if (!empty($category['children'])){
                    printChildCategory($category, $html, $category_id, $level, $parts);
                }
                $html .= '</li>';
            }
        }
        $html .= '</ul></div>';
    }
}

if($registry->has('theme_options') == false) {
    header("location: themeinstall/index.php");
    exit;
}



$theme_options = $registry->get('theme_options'); ?>

<div class="box box-with-categories">
    <div class="box-heading"><?php echo $heading_title; ?></div>
    <div class="strip-line"></div>
    <div class="box-content box-category">
        <?php

        foreach($categories as $category):?>
            <ul class="accordion" id="accordion-category">
                <li class="panel">
                    <?php if ($category['category_id'] == $category_id) { ?>
                        <a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                    <?php } ?>
                    <?php if (!empty($category['children'])): ?>
                        <span class="head"><a style="float:right;padding-right:5px" class="accordion-toggle <?php echo in_array($category['category_id'],$parts)? 'collapse' : 'collapsed'; ?>" data-toggle="collapse"
                                              href="#category<?php echo $category['category_id']?>"><span
                                    class="plus">+</span><span class="minus">-</span></a></span>
                    <?php endif; ?>
                    <?php
                    $categoryHtml = '';
                    $level = 0;
                    printChildCategory($category, $categoryHtml, $category_id, $level, $parts);
                    echo  $categoryHtml;
                    ?>
                </li>
            </ul>
            <?php
        endforeach;

        ?>

    </div>
</div>
