<?php echo $header;
if(isset($mfilter_json)) {
	if(!empty($mfilter_json)) {
		echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>';
	}
}

$theme_options = $registry->get('theme_options');
$config = $registry->get('config');
include('catalog/view/theme/'.$config->get($config->get('config_theme') . '_directory').'/template/new_elements/gallery_wrapper_top.tpl'); 

?>

<div class="product-list product-grid">
	<div class="row">
		<?php
		$row_fluid = 0;
		$row = $theme_options->get( 'product_per_pow2' );
		foreach($images as $image) { $row_fluid++; ?>
			<?php $r=$row_fluid-floor($row_fluid/$row)*$row; if($row_fluid>$row && $r == 1) { echo '</div><div class="row">'; } ?>
				<div class="col-sm-4 col-xs-6">
					 <a href="<?php echo 'image/'.$image['image']?>"  data-image="<?php echo $image['thumb']; ?>" class="popup-image">
						 <img src="<?php echo $image['thumb']?>" />
					 </a>
				</div>
		<?php } ?>
	</div>
</div>


<script>
	$(document).ready(function() {
		$(document).ready(function(){
			$('.popup-image').magnificPopup({
				type: 'image',
				gallery: {enabled: true},
				removalDelay: 500,
				mainClass: 'mfp-fade'
			});
		});
	});


</script>

<?php include('catalog/view/theme/'.$config->get($config->get('config_theme') . '_directory').'/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>
