<?php echo $header; ?><?php echo $column_left; ?>
  <div id="content">
    <div class="page-header">
      <div class="container-fluid">
        <div class="pull-right">
          <button type="submit" form="form-gallery" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
          <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="container-fluid">
      <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
      <?php } ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
        </div>
        <div class="panel-body">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-gallery" class="form-horizontal">
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
              <div class="col-sm-10">
                <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                <?php if ($error_name) { ?>
                  <div class="text-danger"><?php echo $error_name; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_title; ?>"><?php echo $entry_title; ?></span></label>
              <div class="col-sm-10">
                <?php foreach ($languages as $language) { ?>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" />
                    </span>
                    <input type="text" name="module_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($module_description[$language['language_id']]) ? $module_description[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
                  </div>
                <?php } ?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
              <div class="col-sm-10">
                <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                <?php if ($error_keyword) { ?>
                  <div class="text-danger"><?php echo $error_keyword; ?></div>
                <?php } ?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
              <div class="col-sm-10">
                <select name="status" id="input-status" class="form-control">
                  <?php if ($status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>



            <div class="tab-pane" id="tab-image">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_image; ?></td>
                  </tr>
                  </thead>

                  <tbody>
                  <tr>
                    <td class="text-left"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" /></td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <div class="table-responsive">
                <table id="images" class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-left"><?php echo $entry_additional_image; ?></td>
                    <td class="text-right"><?php echo $entry_sort_order; ?></td>
                    <td></td>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $image_row = 0; ?>
                  <?php foreach ($simple_gallery_images as $simple_gallery_image) { ?>
                    <tr id="image-row<?php echo $image_row; ?>">
                      <td class="text-left">
                        <a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail">
                          <img src="<?php echo $simple_gallery_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                        </a>
                        <input type="hidden" name="simple_gallery_image[<?php echo $image_row; ?>][image]" value="<?php echo $simple_gallery_image['image']; ?>" id="input-image<?php echo $image_row; ?>" />
                      </td>
                      <td class="text-right">
                        <input type="text" name="simple_gallery_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $simple_gallery_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" />
                      </td>
                      <td class="text-left">
                        <button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger">
                          <i class="fa fa-minus-circle"></i>
                        </button>
                      </td>
                    </tr>
                    <?php $image_row++; ?>
                  <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <td colspan="2"></td>
                    <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>





  </div>




  <script type="text/javascript"><!--
    var image_row = <?php echo $image_row; ?>;

    function addImage() {
      html  = '<tr id="image-row' + image_row + '">';
      html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="simple_gallery_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
      html += '  <td class="text-right"><input type="text" name="simple_gallery_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
      html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html += '</tr>';

      $('#images tbody').append(html);

      image_row++;
    }
    //--></script>



  <!-- Multiple images select and insert into product form  -->
  <script>
    $(document).ready(function() {

      // Green button - Insert images into form - default order
      $(document).on('click', '#multiple-insert', function() {

        var images = [];
        $('#image-to-select:checked').each(function(i){
          images[i] = $(this).val();
        });

        $('#input-image').val(images[0]);
        $('#thumb-image img').attr('src', '../image/' + images[0]).css('max-width', '100px');

        for(var i = 0; i < images.length-1; i++) {
          addImage();
          $('#input-image' + i).val(images[i+1]);
          $('#thumb-image' + i + ' img').attr('src', '../image/' + images[i+1]).css('max-width', '100px');
        }

        $('#modal-image').modal('hide');

      })
    });


    // Adding images while choosing main image
    $(document).ready(function() {

      $(document).on('click', '#select-main', function () {

        var main_image = $(this).parent().find('input').val();

        // Getting checked images
        var images = [];
        $('#image-to-select:checked').each(function(i){
          images[i] = $(this).val();
        });

        // Remove main image from array of checked ones
        var index = images.indexOf(main_image);
        if (index > -1) {
          images.splice(index, 1);
        }
        console.log(images + images.length)

        // Adding main image into form
        $('#input-image').val(main_image);
        $('#thumb-image img').attr('src', '../image/' + main_image).css('max-width', '100px');


        // Additional images to form
        if(images.length === 1) {
          addImage();
          $('#input-image0').val(images[0]);
          $('#thumb-image0' + ' img').attr('src', '../image/' + images[0]).css('max-width', '100px');

        } else {
          for(var i = 0; i <= images.length-1; i++) {

            addImage();
            $('#input-image' + i).val(images[i]);
            $('#thumb-image' + i + ' img').attr('src', '../image/' + images[i]).css('max-width', '100px');
          }
        }

        $('#modal-image').modal('hide');

      });


    });

  </script>

<?php echo $footer; ?>