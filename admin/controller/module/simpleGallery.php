<?php
class ControllerModuleSimpleGallery extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('module/simpleGallery');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module');
		$this->load->model('catalog/simple_gallery');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			if (!isset($this->request->get['module_id'])) {
				echo 'id: '.$this->model_extension_module->addModule('simpleGallery', $this->request->post);
				$last_module_id = $this->db->getLastId();
				$this->model_catalog_simple_gallery->addSimpleGallery($last_module_id,$this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
				$simple_gallery_id =  $this->model_catalog_simple_gallery->getIdSimpleGalleryByModule($this->request->get['module_id']);
				$this->model_catalog_simple_gallery->editSimpleGallery($simple_gallery_id, array_merge($this->request->post,$this->request->get));
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_additional_image'] = $this->language->get('entry_additional_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['help_title'] = $this->language->get('help_title');
		$data['button_image_add'] = $this->language->get('button_image_add');

		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['help_keyword'] = $this->language->get('help_keyword');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/simpleGallery', 'token=' . $this->session->data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/simpleGallery', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('module/simpleGallery', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('module/simpleGallery', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
			$simple_gallery_id =  $this->model_catalog_simple_gallery->getIdSimpleGalleryByModule($this->request->get['module_id']);
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['module_description'])) {
			$data['module_description'] = $this->request->post['module_description'];
		} elseif (!empty($module_info)) {
			$data['module_description'] = $module_info['module_description'];
		} else {
			$data['module_description'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}



		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
//			echo  $this->request->get['module_id'];
//			$simple_gallery_info = $this->model_catalog_simple_gallery->getSimpleGallery($this->request->get['module_id']);
			$simple_gallery_info = $this->model_catalog_simple_gallery->getSimpleGallery($simple_gallery_id);
		}
//		print_r($simple_gallery_info);
//		die;



		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($this->request->get['module_id'])) {
			$data['keyword'] =  $this->model_catalog_simple_gallery->getKeyword($this->request->get['module_id']);
		} else {
			$data['keyword'] = '';
		}


		// Image
		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($simple_gallery_info)) {
			$data['image'] = $simple_gallery_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($simple_gallery_info) && is_file(DIR_IMAGE . $simple_gallery_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($simple_gallery_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		// Images
		if (isset($this->request->post['simple_gallery_image'])) {
			$simple_gallery_images = $this->request->post['simple_gallery_image'];
		} elseif (isset($simple_gallery_id)) {
			$simple_gallery_images = $this->model_catalog_simple_gallery->getSimpleGalleryImages($simple_gallery_id);
		} else {
			$simple_gallery_images = array();
		}

//		print_r($simple_gallery_images );
//		die;

		$data['simple_gallery_images'] = array();

		foreach ($simple_gallery_images as $simple_gallery_image) {
			if (is_file(DIR_IMAGE . $simple_gallery_image['image'])) {
				$image = $simple_gallery_image['image'];
				$thumb = $simple_gallery_image['image'];
			} else {
				$image = '';
				$thumb = 'no_image.png';
			}

			$data['simple_gallery_images'][] = array(
				'image'      => $image,
				'thumb'      => $this->model_tool_image->resize($thumb, 100, 100),
				'sort_order' => $simple_gallery_image['sort_order']
			);
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();


		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');





		$this->response->setOutput($this->load->view('module/simpleGallery.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/banner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (utf8_strlen($this->request->post['keyword']) > 0) {
			$this->load->model('catalog/url_alias');

			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

//			print_r($this->request->get['module_id']);
//			die;
			if ($url_alias_info && isset($this->request->get['module_id']) && $url_alias_info['query'] != 'gallery_id=' . $this->request->get['module_id']) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}

			if ($url_alias_info && !isset($this->request->get['module_id'])) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}
		}

		return !$this->error;
	}
	
	

}