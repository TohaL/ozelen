<?php

$_['module_heading_title']    = 'Advanced Grid';
$_['text_module']    = 'Module';
$_['text_add_module']    = 'Add Module';
$_['text_add_column']    = 'Add Column';
$_['text_settings']    = 'Settings';
$_['text_column']    = 'Column';
$_['text_design_settings']    = 'Design settings';
$_['text_add_custom_class']    = 'Add custom class';
$_['text_margins']    = 'Margins';
$_['text_paddings']    = 'Paddings';
$_['text_force_full_width']    = 'Force full width';
$_['text_background_color']    = 'Background color';
$_['text_background_image_type']    = 'Background image type';
$_['text_background_image']    = 'Background image';
$_['text_position']    = 'Position';
$_['text_repeat']    = 'Repeat';
$_['text_attachment']    = 'Attachment';
$_['text_layout_settings']    = 'Layout settings';
$_['text_layout']    = 'Layout';
$_['text_position']    = 'Position';
$_['text_status']    = 'Status';
$_['text_sort_order']    = 'Sort Order';
$_['text_disable_on_mobile']    = 'Disable on mobile';
$_['text_yes']    = 'Yes';
$_['text_no']    = 'No';
$_['text_none']    = 'None';
$_['text_standard']    = 'Standard';
$_['text_parallax']    = 'Parallax';
$_['text_top_left']    = 'Top left';
$_['text_top_center']    = 'Top center';
$_['text_top_right']    = 'Top right';
$_['text_bottom_left']    = 'Bottom left';
$_['text_bottom_center']    = 'Bottom center';
$_['text_bottom_right']    = 'Bottom right';
$_['text_enabled']    = 'Enabled';
$_['text_disabled']    = 'Disabled';
$_['text_column_width']    = 'Column width';
$_['text_on_extra_small_devices']    = 'on extra small devices';
$_['text_on_small_devices']    = 'on small devices';
$_['text_on_medium_devices']    = 'on medium devices';
$_['text_on_large_devices']    = 'on large devices';
$_['text_sort']    = 'Sort';
$_['text_type_column']    = 'Type column';


?>




