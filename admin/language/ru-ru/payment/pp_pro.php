<?php
// Heading
$_['heading_title']					= 'PayPal Pro';

// Text
$_['text_success']					= 'Настройки успешно изменены!';
$_['text_edit']                     = 'Редактирование';
$_['text_pp_pro']					= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Авторизация';
$_['text_sale']						= 'Продажа';

// Entry
$_['entry_username']				= 'API Имя пользователя';
$_['entry_password']				= 'API Пароль';
$_['entry_signature']				= 'API Подпись';
$_['entry_test']					= 'Режим отладки';
$_['entry_transaction']				= 'Метод транзакции';
$_['entry_total']					= 'Нижняя граница';
$_['entry_order_status']			= 'Статус заказа после оплаты';
$_['entry_geo_zone']				= 'Географическая зона';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортировки';

// Help
$_['help_test']						= 'Использовать живой шлюз сервера или режим тестирования (песочница) для обработки транзакций?';
$_['help_total']					= 'Проверка всего заказа должна производиться до того как этот способ оплаты станет активным';

// Error
$_['error_permission']				= 'У Вас нет прав для управления данным модулем!';
$_['error_username']				= 'Необходимо заполнить API Имя пользователя!';
$_['error_password']				= 'Необходимо заполнить API Пароль!';
$_['error_signature']				= 'Необходимо заполнить API Подпись!';

