<?php
// Имя модуля, такой он будет виден на вкладке «модули» в панели управления и внутри настроек модуля
$_['heading_title']    = 'Simple Gallery';

//Текст внутри настроек модуля
$_['text_module']      = 'Модули';
$_['text_edit']        = 'Настройки модуля';
$_['entry_status']     = 'Статус';

$_['entry_name']     = 'Название галереи';
$_['entry_title']     = 'Заголовок галереи';
$_['entry_image']     = 'Фото галереи';
$_['entry_additional_image']     = 'Дополнительное изображение';
$_['entry_sort_order']     = 'Порядок сортировки';
$_['button_remove']     = 'Удалить';
$_['help_title']     = 'Отсавьте пустым, если заголовок не нужен для отображения';
$_['button_image_add']     = 'Добавить';
$_['entry_keyword']      = 'SEO URL';
$_['button_image_add']     = 'Добавить';


// Help
$_['help_keyword']       = 'Должно быть уникальным на всю систему и без пробелов';

// Error
$_['error_keyword']      = 'SEO URL занят!';