<?php

// Heading Goes here:
$_['heading_title']    = '<b>Управление блогом</b>';

// Text
$_['text_success']     = 'Успех: Вы изменили модуль управления Блог!';
$_['text_blog_dashboard']   = 'Панель управления Блогом';
$_['text_categories']   = 'Категории';
$_['text_authors']   = 'Авторы';
$_['text_articles']   = 'Статьи';
$_['text_comments']   = 'Комментарии';
$_['text_add_edit_articles']   = 'Добавить/Редактировать Статью';
$_['text_add_edit_categories']   = 'Добавить/Редактировать Категории';
$_['text_add_edit_authors']   = 'Добавить/Редактировать Авторов';
$_['text_add_edit_comments']   = 'Добавить/Редактировать Комментарии';
$_['text_general_settings']   = 'Основные настройки';
$_['text_modules']   = 'Модули';
$_['text_blog_category']   = 'Категории блога';
$_['text_blog_search']   = 'Поиск блога';
$_['text_blog_latest_post']   = 'Последнее сообщение блога';
$_['text_blog_popular_post']   = 'Популярные сообщения блога';
$_['text_blog_product_related_post']   = 'Схожие темы блога продукте в блоге';
$_['text_blog_popular_tags']   = 'Блог Популярные теги';
$_['text_blog_front_information']   = 'Блог доступен здесь <a target="_blank" href="%s">ссылка</a>';


// Error
$_['error_permission'] = 'Внимание: У вас нет разрешения на изменение модуля управления блога!';



?>
