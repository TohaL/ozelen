<?php

// Heading Goes here:
$_['heading_title']    = '<b>Поиск блога</b>';

// Text
$_['text_success']     = 'Успех: Вы изменили модуль поиска по блогам!';

// Error
$_['error_permission'] = 'Внимание: У вас нет разрешения на изменение модуля Blog Search!';

?>
