<?php

$_['module_heading_title']    = 'Настройки сетки';
$_['text_module']    = 'Модуль';
$_['text_add_module']    = 'Добавить модуль';
$_['text_add_column']    = 'Добавить ряд';
$_['text_settings']    = 'Настройки';
$_['text_column']    = 'Колонка';
$_['text_design_settings']    = 'Настройки дизайна';
$_['text_add_custom_class']    = 'Добавить пользовательский класс';
$_['text_margins']    = 'Отступы по краям (Margins)';
$_['text_paddings']    = 'Отступы по краям (Paddings)';
$_['text_force_full_width']    = 'Принудительно по всей ширине';
$_['text_background_color']    = 'Фоновый цвет';
$_['text_background_image_type']    = 'Тип фонового изображения';
$_['text_background_image']    = 'Фоновое изображение';
$_['text_position']    = 'Позиция';
$_['text_repeat']    = 'Повтор фонового изображения (repeat)';
$_['text_attachment']    = 'Привязанность (Attachment)';
$_['text_layout_settings']    = 'Настройки сетки';
$_['text_layout']    = 'Сетка';
$_['text_position']    = 'Позиция';
$_['text_status']    = 'Статус';
$_['text_sort_order']    = 'Порядок сортировки';
$_['text_disable_on_mobile']    = 'Отключить на мобильном телефоне';
$_['text_yes']    = 'Да';
$_['text_no']    = 'Нет';
$_['text_none']    = 'Нет';
$_['text_standard']    = 'Обычный';
$_['text_parallax']    = 'Параллакс';
$_['text_top_left']    = 'Вверху слева';
$_['text_top_right']    = 'Вверху справа';
$_['text_bottom_left']    = 'Снизу слева';
$_['text_bottom_center']    = 'Снизу по центру';
$_['text_bottom_right']    = 'Снизу справа';
$_['text_enabled']    = 'Включен';
$_['text_disabled']    = 'Отключен';
$_['text_column_width']    = 'Ширина колонки';
$_['text_on_extra_small_devices']    = 'на экстра маленьких устройствах';
$_['text_on_small_devices']    = 'на маленьких устройствах';
$_['text_on_medium_devices']    = 'на средних устройствах';
$_['text_on_large_devices']    = 'на больших устройствах';
$_['text_sort']    = 'Сортировка';
$_['text_type_column']    = 'Тип колонки';


?>