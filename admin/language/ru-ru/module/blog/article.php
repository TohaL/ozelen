<?php
// Heading
$_['heading_title']          = 'Статьи';

// Text
$_['text_success']           = 'Успех: Вы изменили статьи!';
$_['text_list']              = 'Список статей';
$_['text_add']               = 'Добавить статью';
$_['text_edit']              = 'Редактировать статьи';
$_['text_default']           = 'По умолчанию';
$_['text_no_selected']       = 'Не выбрано';
$_['text_slider']            = 'Слайдер';
$_['text_classic']           = 'Классик';

// Column
$_['column_title']           = 'Заголовок статьи';
$_['column_date_published']  = 'Дата публикации';
$_['column_author']          = 'Автор';
$_['column_comments']        = 'Комментарии';
$_['column_action']          = 'Действие';

// Tabs
$_['tab_settings']            = 'Настройки';
$_['tab_customization']       = 'Настройка';
$_['tab_related']             = 'Связанный';
$_['tab_gallery']             = 'Галерея';

// Entry
$_['entry_title']             = 'Заголовок статьи';
$_['entry_description']      = 'Описание';
$_['entry_content']          = 'Контент';
$_['entry_meta_title'] 	     = 'Метатег Заголовок';
$_['entry_meta_keyword'] 	 = 'Метатег Ключевые слова';
$_['entry_meta_description'] = 'Метатег Описание';
$_['entry_keyword']          = 'SEO ключевые слова';
$_['entry_parent']           = 'Родитель';
$_['entry_filter']           = 'Фильтры';
$_['entry_store']            = 'Магазины';
$_['entry_image']            = 'Изображение';
$_['entry_thumbnail']        = 'Миниатюра';
$_['entry_youtube']          = 'Youtube';
$_['entry_soundcloud']       = 'SoundCloud';
$_['entry_type']             = 'Тип';
$_['entry_link']             = 'Ссылка';
$_['entry_width']            = 'Ширина';
$_['entry_height']           = 'Высота';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_allow_comments']   = 'Разрешить комментарии';
$_['entry_status']           = 'Статус';
$_['entry_category']         = 'Категория';
$_['entry_author']           = 'Автор';
$_['entry_tags']             = 'Теги';
$_['entry_related_products'] = 'Связанные продукты';
$_['entry_related_articles'] = 'Связанные статьи';
$_['entry_date_added']       = 'Дата добавления';
$_['entry_date_updated']     = 'Дата обновления';
$_['entry_date_published']     = 'Дата публикации';
$_['entry_article_list_gallery_display']     = 'Показать галерею в списке статей';

// Help
$_['help_filter']            = '(Автозаполнение)';
$_['help_tags']              = 'Через запятую.';
$_['help_keyword']           = 'Не используйте пробелы, вместо того, чтобы заменить пробелы с - и убедитесь, что ключевое слово является глобально уникальным.';
$_['help_top']               = 'Дисплей в верхней строке меню. Работает только для топ-родительских статей.';
$_['help_column']            = 'Количество столбцов, чтобы использовать для нижних 3-х статей. Работает только для топ-родительских статей.';
$_['help_category']          = '(Автозаполнение)';
$_['help_related_products']  = '(Автозаполнение)';
$_['help_related_articles']  = '(Автозаполнение)';
$_['help_thumbnail']         = 'Статья миниатюрами будет отображаться в ассоциированных модулей';

// Error
$_['error_warning']          = 'Внимание: Пожалуйста, проверьте форму тщательно на наличие ошибок!';
$_['error_permission']       = 'Внимание: У вас нет разрешения на изменение статей!';
$_['error_title']             = 'Заголовок должен быть от 2 до 32 символов!';
$_['error_meta_title']       = 'Мета Название должно быть больше 3 и меньше 255 символов!';
$_['error_keyword']          = 'SEO ключевое слово уже используется!';