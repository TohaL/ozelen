<?php
// Heading
$_['heading_title']          = 'Настройки';

// Text
$_['text_success']           = 'Успех: вы изменили настройки!';
$_['text_list']              = 'Список категорий';
$_['text_add']               = 'Добавить категорию';
$_['text_edit']              = 'Редактировать категорию';
$_['text_default']           = 'По умолчанию';
$_['text_settings']          = 'Настройки';
$_['text_standard']          = 'Стандарт';
$_['text_ajax']              = 'Ajax (динамическая загрузка)';
$_['text_local']             = 'Локальный';
$_['text_facebook']          = 'Facebook';
$_['text_disqus']            = 'Disqus';

// Column
$_['column_name']            = 'Имя настройки';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'Действие';

// Tabs
$_['tab_article']            = 'Новости';
$_['tab_category']           = 'Категории';
$_['tab_comment']            = 'Комментарии';
$_['tab_author']             = 'Автор';

// Entry
$_['entry_status']                      = 'Статус';
$_['entry_comments_approval']           = 'Одобрение комментариев';
$_['entry_pagination_type']             = 'Тип нумерации страниц';
$_['entry_article_page_limit']          = 'Лимит стриниц статей';
$_['entry_comments_engine']             = 'Система комментариев';
$_['entry_disqus_name']                 = 'Disqus Имя';
$_['entry_facebook_id']                 = 'Facebook ID';
$_['entry_article_list_template']       = 'Шаблон стиска статей';
$_['entry_article_detail_template']     = 'Шюблон Статьи подробнее';
$_['entry_article_related_template']    = 'Шаблон связанных статей';
$_['entry_article_related']             = 'Связанные статьи';
$_['entry_article_scroll_related']      = 'Scroll связанных статей';
$_['entry_article_related_per_row']     = 'Статьи по теме в строке';
$_['entry_product_related']             = 'Связанный продукт';
$_['entry_product_scroll_related']      = 'Scroll Связанных продуктов';
$_['entry_product_related_per_row']     = 'Связанные продукты в строке';
$_['entry_author_description']          = 'Описание автора';
$_['entry_gallery_image_size']          = 'Размер по умолчанию изображения галереи <br>(W x H)';
$_['entry_gallery_youtube_size']        = 'Размер по умолчанию галереи Youtube <br>(W x H)';
$_['entry_gallery_soundcloud_size']     = 'Размер по умолчанию галереи SoundCloud <br>(W x H)';
$_['entry_gallery_related_article_size']= 'Размер по умолчанию связанных галерей <br>(W x H)';

// Help
$_['help_filter']            = '(Автозаполнение)';
$_['help_facebook_id']        = 'Регистрация в окне комментария, затем получение идентификатора приложения через скрипте или Регистрация Facebook 
Application ID, для редактирования комментарии';

// Error
$_['error_warning']          = 'Внимание: Пожалуйста, проверьте форму тщательно на наличие ошибок!';
$_['error_permission']       = 'Внимание: У вас нет разрешения на изменение категории!';
$_['error_name']             = 'Категория Название должно быть от 2 до 32 символов!';
$_['error_meta_title']       = 'Мета Название должно быть больше 3 и меньше 255 символов!';
$_['error_keyword']          = 'SEO ключевое слово уже используется!';