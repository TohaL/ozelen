<?php
// Heading
$_['heading_title']      = 'Комментарии';

// Text
$_['text_success']       = 'Успех: Вы изменили комментарии!';
$_['text_list']          = 'Список комментариев';
$_['text_add']           = 'Добавить комментарий';
$_['text_edit']          = 'Редактировать комментарий';
$_['text_default']       = 'По умолчанию';
$_['text_percent']       = 'Процент';
$_['text_amount']        = 'Фиксированное количество';

// Column
$_['column_content']     = 'Содержание';
$_['column_name']        = 'Имя автора';
$_['column_article']     = 'Статья';
$_['column_email']       = 'E-mail автора';
$_['column_date_added']  = 'Дата добавления';
$_['column_sort_order']  = 'Порядок сортировки';
$_['column_status']      = 'Статус';
$_['column_action']      = 'Действие';

// Entry
$_['entry_name']         = 'Имя автора';
$_['entry_email']        = 'E-mail автора';
$_['entry_article']      = 'Статья';
$_['entry_content']      = 'Содержание';
$_['entry_status']       = 'Статус';
$_['entry_sort_order']   = 'Порядок сортировки';
$_['entry_type']         = 'Тип';

// Help
$_['help_keyword']       = 'Не используйте пробелы, вместо того, чтобы заменить пробелы - и убедитесь, что ключевое слово является глобально уникальным.';

// Error
$_['error_permission']   = 'Внимание: У вас нет разрешения на изменение комментарии!';
$_['error_name']         = 'Имя комментария должно быть от 2 до 255 символов!';