<?php
class ModelCatalogSimpleGallery extends Model {
	public function addSimpleGallery($module_id,$data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "simple_gallery SET date_added = NOW(), module_id = '".(int)$module_id."'");

		$simple_gallery_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "simple_gallery SET image = '" . $this->db->escape($data['image']) . "' WHERE simple_gallery_id = '" . (int)$simple_gallery_id . "'");
		}

		if (isset($data['simple_gallery_image'])) {
			foreach ($data['simple_gallery_image'] as $simple_gallery_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "simple_gallery_image SET simple_gallery_id = '" . (int)$simple_gallery_id . "', image = '" . $this->db->escape($simple_gallery_image['image']) . "', sort_order = '" . (int)$simple_gallery_image['sort_order'] . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'gallery_id=" . (int)$simple_gallery_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}


		$this->cache->delete('simple_gallery');
		return $simple_gallery_id;
	}

	public function editSimpleGallery($simple_gallery_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "simple_gallery SET date_modified = NOW() WHERE simple_gallery_id = '" . (int)$simple_gallery_id. "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "simple_gallery SET image = '" . $this->db->escape($data['image']) . "' WHERE simple_gallery_id = '" . (int)$simple_gallery_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "simple_gallery_image WHERE simple_gallery_id = '" . (int)$simple_gallery_id . "'");

		if (isset($data['simple_gallery_image'])) {
			foreach ($data['simple_gallery_image'] as $simple_gallery_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "simple_gallery_image SET simple_gallery_id = '" . (int)$simple_gallery_id . "', image = '" . $this->db->escape($simple_gallery_image['image']) . "', sort_order = '" . (int)$simple_gallery_image['sort_order'] . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'gallery_id=" . (int)$simple_gallery_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'gallery_id=" . (int)$data['module_id'] . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('simple_gallery');
	}

	public function deleteSimpleGallery($simple_gallery_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "simple_gallery WHERE simple_gallery_id= '" . (int)$simple_gallery_id. "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "simple_gallery_image WHERE simple_gallery_id = '" . (int)$simple_gallery_id . "'");
		$this->cache->delete('simple_gallery');
	}

	public function removeSimpleGallery($simple_gallery_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "simple_gallery WHERE simple_gallery_id= '" . (int)$simple_gallery_id. "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "simple_gallery_image WHERE simple_gallery_id = '" . (int)$simple_gallery_id . "'");
		$this->cache->delete('simple_gallery');
	}

	public function deleteModule($module_id) {
		$simple_gallery_id =  $this->model_catalog_simple_gallery->getIdSimpleGalleryByModule($module_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "simple_gallery WHERE simple_gallery_id= '" . (int)$simple_gallery_id. "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "simple_gallery_image WHERE simple_gallery_id = '" . (int)$simple_gallery_id . "'");
	}

	public function getKeyword($module_id){
		$query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'gallery_id=" . (int)$module_id. "'" );
		if (isset($query->row['keyword'])){
			return $query->row['keyword'];
		} else {
			return '';
		}
	}

	public function getSimpleGallery($simple_gallery_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "simple_gallery p WHERE p.simple_gallery_id = '" . (int)$simple_gallery_id . "' ");

		return $query->row;
	}

	public function getIdSimpleGalleryByModule($module_id) {
		$query = $this->db->query("SELECT DISTINCT simple_gallery_id FROM " . DB_PREFIX . "simple_gallery p WHERE p.module_id = '" . (int)$module_id. "'");

		if (!empty($query->row)){
			return $query->row['simple_gallery_id'];
		} else {
			return false;
		}
	}

	public function getSimpleGalleries($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "simple_gallery p ";

		$sql .= " GROUP BY p.simple_gallery_id";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getSimpleGalleryImages($simple_gallery_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "simple_gallery_image WHERE simple_gallery_id = '" . (int)$simple_gallery_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getTotalSimpleGallerys($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.simple_gallery_id) AS total FROM " . DB_PREFIX . "simple_gallery p";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

}
